/*
SQLyog Ultimate - MySQL GUI v8.21 
MySQL - 5.6.12-log : Database - default
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`default` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `default`;

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `ci_sessions` */

/*Table structure for table `meta` */

DROP TABLE IF EXISTS `meta`;

CREATE TABLE `meta` (
  `meta_id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_title` varchar(66) DEFAULT NULL,
  `meta_keywords` varchar(80) DEFAULT NULL,
  `meta_desc` varchar(165) DEFAULT NULL,
  `meta_robots` enum('index, follow','noindex, nofollow') DEFAULT 'index, follow',
  `uri_segment` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  UNIQUE KEY `uri_segment` (`uri_segment`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `meta` */

insert  into `meta`(`meta_id`,`meta_title`,`meta_keywords`,`meta_desc`,`meta_robots`,`uri_segment`) values (1,'Raenz Online Shop','Toko Online Shop Belanja Pemesanan Murah Transfer Iklan Raenz','Toko Online','index, follow','default');

/*Table structure for table `setting` */

DROP TABLE IF EXISTS `setting`;

CREATE TABLE `setting` (
  `email` varchar(255) NOT NULL DEFAULT 'angraenz@gmail.com',
  `mobile_phone_number` varchar(255) NOT NULL DEFAULT '-',
  `pin_bb` varchar(8) NOT NULL DEFAULT '-',
  `website_name` varchar(100) NOT NULL DEFAULT 'raenz website',
  `shortcut_icon` varchar(255) NOT NULL,
  `use_recaptcha` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `recaptcha_public_key` varchar(255) NOT NULL,
  `recaptcha_private_key` varchar(255) NOT NULL,
  `fb_app_id` varchar(255) NOT NULL,
  `fb_app_secret` varchar(255) NOT NULL,
  `ga_id` varchar(255) NOT NULL,
  `ga_domain_name` varchar(255) NOT NULL,
  `template` enum('1','2') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `setting` */

insert  into `setting`(`email`,`mobile_phone_number`,`pin_bb`,`website_name`,`shortcut_icon`,`use_recaptcha`,`recaptcha_public_key`,`recaptcha_private_key`,`fb_app_id`,`fb_app_secret`,`ga_id`,`ga_domain_name`,`template`) values ('angraenz@gmail.com','+6283872989393','2A57D770','Raenz Things','Capture5.PNG','NO','6LcaVtwSAAAAAF49OT-h1BJP__o033ZUftesT55W','6LcaVtwSAAAAAMyDWI_9Rjk8WBmvskr3tVShXK2o','427938927275194','88152240cad3ff833de64c435b81aaf0','UA-38553884-1','raenz.com','1');

/*Table structure for table `slider` */

DROP TABLE IF EXISTS `slider`;

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `show_title` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `is_a_link` enum('YES','NO') DEFAULT 'NO',
  `link_url` varchar(250) DEFAULT NULL,
  `slider_caption` text,
  `slider_image` varchar(250) NOT NULL,
  `slider_image_thumbs` varchar(250) NOT NULL,
  `image_used` enum('FILE','URL') DEFAULT 'FILE',
  `image_url` varchar(500) DEFAULT NULL,
  `image_url_thumbs` varchar(500) DEFAULT NULL,
  `active` enum('YES','NO') NOT NULL DEFAULT 'YES',
  PRIMARY KEY (`slider_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `slider` */

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `datetime` datetime NOT NULL,
  `desc` text,
  `image` varchar(500) DEFAULT NULL,
  `image_thumbs` varchar(500) DEFAULT NULL,
  `image_thumbs2` varchar(500) DEFAULT NULL,
  `image_thumbs3` varchar(500) DEFAULT NULL,
  `image2` varchar(500) DEFAULT NULL,
  `image2_thumbs` varchar(500) DEFAULT NULL,
  `image2_thumbs2` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `test` */

/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(50) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `user_type` */

insert  into `user_type`(`user_type_id`,`user_type`) values (1,'superadmin'),(2,'admin'),(3,'user');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` enum('YES','NO') COLLATE utf8_bin NOT NULL DEFAULT 'NO',
  `banned` enum('YES','NO') COLLATE utf8_bin NOT NULL DEFAULT 'NO',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0.0.0.0',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` int(11) NOT NULL DEFAULT '2',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `users` */

insert  into `users`(`user_id`,`password`,`email`,`activated`,`banned`,`ban_reason`,`new_password_key`,`new_password_requested`,`new_email`,`new_email_key`,`last_ip`,`last_login`,`created`,`modified`,`type`) values (8,'1b5c1c7c43a9da10029b87c7558bdbf4aad19864','if07087@gmail.com','YES','NO','',NULL,NULL,NULL,NULL,'::1','2014-01-10 07:58:59','2014-01-02 07:33:10','2014-01-02 07:33:12',1),(9,'24816124045e329ca80df9060ab50f414130d7a8','test@gmail.com','YES','NO','',NULL,NULL,NULL,NULL,'::1','2014-01-02 00:45:50','2014-01-02 07:43:25','2014-01-02 07:43:26',3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
