function ajaxfilemanager(field_name, url, type, win) {
    var ajaxfilemanagerurl = BASE_URL+"assets/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
    var view = 'detail';
    switch (type) {
        case "image":
            view = 'thumbnail';
            break;
        case "media":
            view = 'thumbnail';
            break;
        case "flash":
            view = 'thumbnail';
            break;
        case "file":
            view = 'thumbnail';
            break;
        default:
            return false;
    }
    tinyMCE.activeEditor.windowManager.open({
        url: BASE_URL+"assets/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
        width: 782,
        height: 440,
        inline : "yes",
        close_previous : "yes"
    },{
        window : win,
        input : field_name
    });
            
/*            return false;			
			var fileBrowserWindow = new Array();
			fileBrowserWindow["file"] = ajaxfilemanagerurl;
			fileBrowserWindow["title"] = "Ajax File Manager";
			fileBrowserWindow["width"] = "782";
			fileBrowserWindow["height"] = "440";
			fileBrowserWindow["close_previous"] = "no";
			tinyMCE.openWindow(fileBrowserWindow, {
			  window : win,
			  input : field_name,
			  resizable : "yes",
			  inline : "yes",
			  editor_id : tinyMCE.getWindowArg("editor_id")
			});
			
			return false;*/
}