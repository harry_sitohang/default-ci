myTinyMCE = {
    tinyMCEObj : null,
    options : {
        mode : "exact",
        relative_urls : false,
        convert_urls : false,
        elements : "rte",
        theme : "advanced",
        skin : "o2k7",
        plugins : "advimage,advlink,media,contextmenu,emotions,fullscreen,save,table,print",
        theme_advanced_buttons1_add_before : "save,newdocument,fullscreen,print,separator",
        theme_advanced_buttons1_add : "",
        theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle,separator,table",
        theme_advanced_buttons2_add_before: "cut,copy,separator,",
        theme_advanced_buttons3_add_before : "fontselect,fontsizeselect",
        theme_advanced_buttons3_add : "media,emotions",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        extended_valid_elements : "hr[class|width|size|noshade]",
        file_browser_callback : "ajaxfilemanager",
        paste_use_dialog : false,
        theme_advanced_resizing : true,
        //theme_advanced_statusbar_location : "bottom",
        //theme_advanced_resize_horizontal : true,
        //theme_advanced_resize_vertical : true,
        apply_source_formatting : true,
        force_br_newlines : true,
        force_p_newlines : false,
        content_css : ASSETS_CSS+'all.css'
    },
    initTinyMCE : function(option){
        $.extend(this.options, option);
//        if(typeof(oTable) !== 'undefined' && oTable.request_type.match(/^(ajax|tab)$/i)){
//            this.options.theme_advanced_buttons1_add_before = "newdocument,fullscreen,print,separator";
//        }
        this.tinyMCEObj = tinyMCE.init(this.options);
    }
};



