if (typeof (oTable) === 'undefined') {
    oTable = {
        grid_id: '#datatable',
        grid: null,
        grid_config: null,
        request_type: null,
        datatable_modal: '#datatable-modal',
        datatable_tab: '#datatable-tab',
        add_tab: '#add-tab',
        fields: null,
        scroll_margin_top: typeof (SCROLL_MARGIN_TOP) !== "undefined" ? SCROLL_MARGIN_TOP : 0,
        loading_animation_process: '<span class="loading-oTable-process">&nbsp;<img src="' + ASSETS_URL + 'img/loading.gif">&nbsp;loading&nbsp;</span>',
        init: function() {
            this.load_grid();
            this.load_fields();



            var oTable_object = this;

            $('.refresh').live('click', function(event) {
                event.preventDefault();
                oTable_object.refresh();
            });

            $('[action=dt-del]').live('click', function(event) {
                event.preventDefault();
                if (confirm("Are you sure want to delete this row ?")) {
                    oTable_object.action_handler($(this).attr('href'), 'del');
                }
            });


            $('[action=dt-edit]').live('click', function(event) {
                event.preventDefault();
                oTable_object.action_handler($(this).attr('href'), 'edit');
            });


            $('[action=dt-add]').live('click', function(event) {
                event.preventDefault();
                oTable_object.action_handler($(this).attr('href'), 'add');
            });

            if ($(oTable_object.datatable_modal).length != 0) {
                $(".close-datatable-modal").live('click', function(event) {
                    event.preventDefault();
                    $(oTable_object.datatable_modal).modal('hide');
                });

                $(oTable_object.datatable_modal).find("#btn-datatable-action").live('click', function(event) {
                    event.preventDefault();
                    var action = $(this).attr('action');
                    oTable_object.datatable_action_clicked(action);
                });

                $(oTable_object.datatable_modal + " input[type=text], " + oTable_object.datatable_modal + " input[type=password]").live('keyup', function(event) {
                    event.preventDefault();
                    if (typeof ($(this).attr("plugin")) !== "undefined" && $(this).attr("plugin").match(/char_count/i)) {
                        oTable_object.char_count_event_handler($(this));
                    }
                    if (event.keyCode == 13) {
                        var action = $(oTable_object.datatable_modal).find("#btn-datatable-action").attr('action');
                        oTable_object.datatable_action_clicked(action);
                    }
                });

                //add event char_count
                $(oTable_object.datatable_modal + " input[type=text], " + oTable_object.datatable_modal + " input[type=password]").trigger("keyup");
            }

            if ($(oTable_object.datatable_tab).length != 0) {
                $(oTable_object.datatable_tab + ' a:first').tab('show');

                $(oTable_object.datatable_tab + ' a').live('click', function(e) {
                    e.preventDefault();
                    $(this).tab('show');
                });

                //oTable.show_tab(CLASS_URL+"add", true); 

                $('.tab-content').find("#btn-datatable-action").live('click', function(event) {
                    event.preventDefault();
                    var action = $(this).attr('action');
                    if ($(this).hasClass("disabled"))
                        return false;
                    oTable_object.datatable_action_clicked(action);
                });

                $('.tab-content input[type=text], .tab-content input[type=password]').live('keyup', function(event) {
                    if (typeof ($(this).attr("plugin")) !== "undefined" && $(this).attr("plugin").match(/char_count/i)) {
                        oTable_object.char_count_event_handler($(this));
                    }
                    if (event.keyCode == 13) {
                        var action = $('.tab-content').find("#btn-datatable-action").attr('action');
                        oTable_object.datatable_action_clicked(action);
                    }
                });

                //add event char_count
                $('.tab-content input[type=text], .tab-content input[type=password]').trigger("keyup");
            }

            $("[rel=tooltip]").live("mouseover", function() {
                $(this).tooltip({
                    placement: 'top',
                    trigger: 'hover',
                    title: $(this).attr('title')
                });
            });


            $("[action=dt-export-xls]").live("click", function(e) {
                e.preventDefault();
                //oTable_object.report('xls');
                window.location = $(this).attr("href") + '?sSearch=' + $('#datatable_filter input').val();
            });


        },
        char_count_event_handler: function(elm) {
            if (elm.parent(".controls").find(".char_count").length == 0) {
                elm.parent(".controls").append('<span class="char_count badge">' + elm.val().length + '</span>');
            } else {
                elm.parent(".controls").find(".char_count").removeClass("badge-success");
                elm.parent(".controls").find(".char_count").removeClass("badge-important");
                elm.parent(".controls").find(".char_count").html(elm.val().length);
            }

            if (elm.val().length >= elm.attr("min_length") && elm.val().length <= elm.attr("max_length")) {
                elm.parent(".controls").find(".char_count").addClass("badge-success");
            } else {
                elm.parent(".controls").find(".char_count").addClass("badge-important");
            }
        },
        load_grid: function() {
            var oTable_object = this;
            $.ajax({
                url: CLASS_URL + "dt_config_template",
                type: 'post',
                dataType: 'json',
                error: function(request) {
                    console.log(request.responceText);
                },
                success: function(json) {
                    //SET DATATABLE CONFIG
                    oTable_object.grid_config = json.dt_config;

                    //GET DATATABLE DATA BY SERVER SIDE PROCESSING
                    oTable_object.grid = $(oTable_object.grid_id).dataTable(oTable_object.grid_config);

                    // ADD BUTTON SEARCH
                    $("div" + oTable_object.grid_id + "_filter").append('<i class="icon-search" style="margin-top:5px;"></i>&nbsp;');
                    $("div" + oTable_object.grid_id + "_filter").find("input").attr('placeholder', 'Type to search then press ENTER');
                    $("div" + oTable_object.grid_id + "_filter").find("input").addClass('span3');
                    //$("div"+oTable_object.grid_id+"_filter").append('<button class="btn btn-primary pull-right" id="search-oTable"><i class="icon-search icon-white"></i></button>');

                    // ADD BUTTON SEARCH EVENT - KEYUP 'ENTER' EVENT
                    //$(oTable_object.grid_id).dataTable().fnSetFilteringDelay();
                    $(oTable_object.grid_id).dataTable().fnFilterOnButton();

                    oTable_object.request_type = json.additional_parameter.request_type;
                    oTable_object.scroll_margin_top = oTable_object.request_type.match(/^tab$/i) ? oTable_object.scroll_margin_top : 0;
                    if (typeof (myTinyMCE) !== 'undefined') {
                        $.extend(myTinyMCE, {
                            initTinyMCE: function(option) {
                                $.extend(myTinyMCE.options, option);
                                if (oTable.request_type.match(/^(ajax|tab)$/i)) {
                                    myTinyMCE.options.theme_advanced_buttons1_add_before = "newdocument,fullscreen,print,separator";
                                }
                                myTinyMCE.tinyMCEObj = tinyMCE.init(myTinyMCE.options);
                            }
                        });
                    }
                }
            });
        },
        refresh: function() {
            this.grid.fnDraw(false);
        },
        report: function(type) {
            var oTable_object = this;
            $.extend(oTable_object.grid_config, {
                fnServerData: function(sSource, aoData, fnCallback, oSettings) {
                    /* Add some extra data to the sender */
                    aoData.push({
                        "name": "report",
                        "value": (typeof (type) === 'undefined') ? "xls" : type
                    });


                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": function(json) {
                            if (firstDraw) {
                                oSettings._iDisplayLength = json.aaData.length;
                            }
                            fnCallback(json);
                        }
                    });
                }
            });
            console.log(JSON.stringify(oTable_object.grid_config.fnServerData));
        //oTable_object.grid = $(oTable_object.grid_id).dataTable(oTable_object.grid_config);
        //this.grid.fnDraw(false);
        },
        load_fields: function() {
            var oTable_object = this;
            $.ajax({
                url: CLASS_URL + "get_fields_data/json",
                type: 'post',
                async: true,
                dataType: 'json',
                error: function(request) {
                    console.log(request.responseText);
                },
                success: function(json) {
                    oTable_object.fields = json;
                }
            });
        },
        datatable_action_clicked: function(action) {

            //            if($("input[type=hidden][name=multipart]").val().match(/^1$/)){
            //                var data = new FormData();
            //                $.each($('input[type=file]')[0].files, function(i, file) {
            //                    data.append($(this).attr('name'), file);
            //                });
            //            }else{
            //                var data = new Object();
            //            }

            var data = new Object();


            //            for(var i=0; i < this.fields.length; i++){
            //                if(this.fields[i].type.match(/^text$/i)){
            //                    data[this.fields[i].name] = tinyMCE.get(this.fields[i].name).getContent();
            //                }else {
            //                    data[this.fields[i].name] = $( "#"+this.fields[i].name ).val();
            //                }
            //                data['action'] = action;
            //            }

            for (var field in this.fields) {
                //                if(this.fields[field].type.match(/^text$/i)){
                //                    if($("input[type=hidden][name=multipart]").val().match(/^1$/)){
                //                        data.append(this.fields[field].name, tinyMCE.get(this.fields[field].name).getContent());
                //                    }else{
                //                        data[this.fields[field].name] = tinyMCE.get(this.fields[field].name).getContent();
                //                    }
                //                }else {
                //                    if($("input[type=hidden][name=multipart]").val().match(/^1$/)){
                //                        data.append(this.fields[field].name, $( "#"+this.fields[field].name ).val());
                //                    }else{
                //                        data[this.fields[field].name] = $( "#"+this.fields[field].name ).val();
                //                    }
                //                }


                if (this.fields[field].type.match(/^text$/i)) {
                    data[this.fields[field].name] = tinyMCE.get(this.fields[field].name).getContent();
                } else {
                    data[this.fields[field].name] = $("#" + this.fields[field].name).val();
                }

                data['action'] = action;
            }


            if (action.match(/^edit|add$/i)) {
                if ($("input[type=hidden][name=multipart]").val() == true) {
                    if (action.match(/^edit$/i)) {
                        data['id'] = $('input[type=hidden][field=pk]').val();
                        if ($("input[type=checkbox][name=change_image]").is(':checked')) {
                            data['change_image'] = true;
                            this.check_image_ajax(data, action);
                        } else {
                            this.edit(data);
                        }
                    } else {
                        this.check_image_ajax(data, action);
                    }

                    return false;
                }

                if (action.match(/^edit$/i)) {
                    data['id'] = $('input[type=hidden][field=pk]').val();
                    this.edit(data);
                } else {
                    this.add(data);
                }
            }

        //tinyMCE.get('edit_news_content').setContent(json.content);
        //console.log(JSON.stringify(data));
        },
        action_handler: function(url, action) {
            if (this.request_type.match(/^normal|modal|tab$/i)) {
                if (this.request_type.match(/^normal/i)) {
                    window.location = url;
                } else if (this.request_type.match(/^modal$/i)) {
                    if (action.match(/^edit|add$/i)) {
                        this.show_modal(url);
                    } else if (action.match(/^del$/i)) {
                        this.del(url);
                    }
                } else if (this.request_type.match(/^tab/i)) {
                    if (action.match(/^edit|add$/i)) {
                        this.show_tab(url);
                    } else if (action.match(/^del$/i)) {
                        this.del(url);
                    }
                }
            } else {
                console.log("could not defined your request type, for request type avaliable 'normal', 'modal', 'tab'");
            }
        },
        show_modal: function(url) {
            var oTable_object = this;
            $.ajax({
                url: url,
                type: 'post',
                async: true,
                dataType: 'json',
                error: function(request) {
                    console.log(request.responseText);
                },
                success: function(json) {
                    $(oTable_object.datatable_modal).find(".modal-body").html(json.modal_body);
                    $(oTable_object.datatable_modal).find(".modal-header .header-title").html(json.action.match(/^edit$/i) ? 'Edit ' + json.modal_header_title : 'Add ' + json.modal_header_title);

                    //SET MODAL ACTION
                    $(oTable_object.datatable_modal).find("#btn-datatable-action").html(json.action.match(/^edit$/i) ? 'Save changes' : 'Add');
                    $(oTable_object.datatable_modal).find("#btn-datatable-action").attr('action', json.action);

                    $(oTable_object.datatable_modal).modal('show');

                    //set field input editor
                    oTable_object.set_field_input_editor();

                    //add event char_count
                    $(oTable_object.datatable_modal + " input[type=text], " + oTable_object.datatable_modal + " input[type=password]").trigger("keyup");
                }
            });

        },
        show_tab: function(url) {
            //init_add = typeof(init_add) !== 'undefined' ? init_add : false; 
            oTable_object = this;
            $.ajax({
                url: url,
                type: 'post',
                async: false,
                dataType: 'json',
                error: function(request) {
                    console.log(request.responseText);
                },
                success: function(json) {

                    //set tab header
                    if ($("ul.nav-tabs" + oTable_object.datatable_tab).find('li a[href="#action-tab"]').length == 0) {
                        $("ul.nav-tabs" + oTable_object.datatable_tab).append('<li><a href="#action-tab">Add</a></li>');
                    }
                    //set tab content
                    if ($('.tab-content').find(".tab-pane#action-tab").length == 0) {
                        $('.tab-content').append('<div class="tab-pane" id="action-tab">' + json.modal_body + '' + json.modal_action + '</div>')
                    }

                    //                    if(json.action.match(/^add$/i) && init_add === true){
                    //                        //$("ul.nav-tabs"+oTable_object.datatable_tab).append('<li><a href="#action-tab">Add</a></li>');
                    //                        //$('.tab-content').append('<div class="tab-pane" id="action-tab">'+json.modal_body+''+json.modal_action+'</div>');
                    //                    }else 
                    if (json.action.match(/^add$/i)) {
                        $("ul.nav-tabs" + oTable_object.datatable_tab).find('a[href=#action-tab]').html('Add');
                        $('.tab-content').find('.tab-pane#action-tab').html(json.modal_body + '' + json.modal_action);
                        $("a[href=#action-tab]").trigger('click');
                    } else if (json.action.match(/^edit/i)) {
                        $("ul.nav-tabs" + oTable_object.datatable_tab).find('a[href=#action-tab]').html(json.tab_li_title);
                        $('.tab-content').find('.tab-pane#action-tab').html(json.modal_body + '' + json.modal_action);
                        $("a[href=#action-tab]").trigger('click');
                    }

                    //set field input editor
                    oTable_object.set_field_input_editor();

                    //add event char_count
                    $('.tab-content input[type=text], .tab-content input[type=password]').trigger("keyup");

                /*else if(json.action.match(/^edit$/i) && $("ul.nav-tabs"+oTable_object.datatable_tab).find('a[href="#'+json.id+'"]').length == 0){
                     $("ul.nav-tabs"+oTable_object.datatable_tab).append('<li><a href="#'+json.id+'">'+ json.tab_li_title+'</a></li>');
                     $('.tab-content').append('<div class="tab-pane" id="'+json.id+'">'+json.modal_body+''+json.modal_action+'</div>');
                     }else if(json.action.match(/^edit$/i) && $("ul.nav-tabs"+oTable_object.datatable_tab).find('a[href="#'+json.id+'"]').length != 0){
                     $('.tab-content').find('.tab-pane#'+json.id).html(json.modal_body+''+json.modal_action);
                     }*/
                }
            });
        },
        set_field_input_editor: function() {
            for (var field in this.fields) {
                if (this.fields[field].type.match(/^text$/i)) {
                    myTinyMCE.initTinyMCE({
                        'elements': this.fields[field].name
                    });
                } else if (this.fields[field].type.match(/^date$/i)) {
                    $("#" + this.fields[field].name).datepicker({
                        dateFormat: 'yy-mm-dd',
                        showButtonPanel: true
                    });
                }
                else if (this.fields[field].type.match(/^datetime|timestamp$/i)) {
                    $("#" + this.fields[field].name).datetimepicker({
                        dateFormat: 'yy-mm-dd',
                        timeFormat: 'HH:mm:ss'
                    });
                }
            }
        },
        check_image_ajax: function(data, action) {
            oTable_object = this;

            var MyFormData = new FormData();
            //            $.each($('input[type=file]'), function(i, files) {
            //                MyFormData.append($(this).attr('name'), $('input[type=file]')[0].files[i]);
            //            });
            $.each($('input[type=file]'), function(i, obj) {
                var input_name = $(this).attr('name');
                $.each(obj.files, function(j, file) {
                    MyFormData.append(input_name, file);
                });
            });

            //console.log(JSON.stringify(MyFormData));

            $.ajax({
                url: CLASS_URL + "check_image_ajax",
                type: 'post',
                data: MyFormData,
                contentType: false,
                processData: false,
                cache: false,
                beforeSend: function() {
                    $("#msg-box").html('');
                },
                dataType: 'json',
                error: function(request) {
                    console.log(request.responseText);
                },
                success: function(json) {
                    var alert_type = json.status;
                    var alert_title = json.status + " information ";

                    if (alert_type == 'success') {
                        $.each(json.files, function(key, val) {
                            data[key] = val;
                        });

                        if (action.match(/^add$/i)) {
                            oTable_object.add(data);
                        } else {
                            oTable_object.edit(data);
                        }
                    } else {
                        alert_msg.init({
                            dom_container: $("#msg-box"),
                            type: alert_type,
                            title: alert_title,
                            msg: json.msg,
                            fill_type: 'replace'
                        });

                        $('html, body').animate({
                            scrollTop: $("ul#datatable-tab").offset().top
                        }, 500);
                    }
                }
            });
        },
        edit: function(data) {
            oTable_object = this;
            $.ajax({
                url: CLASS_URL + "edit/" + data.id,
                type: 'post',
                data: data,
                async: true,
                beforeSend: function() {
                    $("#msg-box").html('');
                    oTable_object.pre_loading_otable_process();
                },
                dataType: 'json',
                error: function(request) {
                    oTable_object.post_loading_otable_process();
                    console.log(request.responseText);
                },
                success: function(json) {
                    oTable_object.post_loading_otable_process();
                    
                    alert_msg.init({
                        dom_container: $("#msg-box"),
                        type: json.status,
                        title: json.status + " information ",
                        msg: json.msg,
                        fill_type: 'replace'
                    });
                    $('html, body').animate({
                        scrollTop: $("ul#datatable-tab").offset().top
                    }, 500);
                    
                    
                    if (json.status == 'success') {
                        if ($("input[type=checkbox][name=change_image]").is(':checked')) {
                            $.each(json.return_images, function(key, val) {
                                $("img[type=image][image=" + key + "]").attr('src', val);
                            });
                        }
                        oTable_object.refresh();
                    } else {
                        for (var key in json.fields) {
                            var field = json.fields[key];
                            if (field['error'] == "") {
                                $("#" + key).parents(".control-group").addClass("success");
                            } else {
                                $("#" + key).parents(".control-group").addClass("error");
                                $("#" + key).parents(".controls").append(field["error"]);
                            }
                        }
                        
                        $('html, body').animate({
                            scrollTop: $(".controls").find(".error-msg").eq(0).offset().top - oTable_object.scroll_margin_top
                        }, 500);
                    }

                }
            });
        },
        add: function(data) {
            oTable_object = this;

            $.ajax({
                url: CLASS_URL + "add",
                type: 'post',
                data: data,
                async: true,
                beforeSend: function() {
                    $("#msg-box").html('');
                    oTable_object.pre_loading_otable_process();
                },
                dataType: 'json',
                error: function(request) {
                    oTable_object.post_loading_otable_process();
                    console.log(request.responseText);
                },
                success: function(json) {
                    oTable_object.post_loading_otable_process();
                    
                    alert_msg.init({
                        dom_container: $("#msg-box"),
                        type: json.status,
                        title: json.status + " information ",
                        msg: json.msg,
                        fill_type: 'replace'
                    });

                    $('html, body').animate({
                        scrollTop: $("ul#datatable-tab").offset().top
                    }, 500);
                        
                    if (json.status == 'success') {
                        oTable_object.refresh();
                    } else {
                        for (var key in json.fields) {
                            var field = json.fields[key];
                            if (field['error'] == "") {
                                $("#" + key).parents(".control-group").addClass("success");
                            } else {
                                $("#" + key).parents(".control-group").addClass("error");
                                $("#" + key).parents(".controls").append(field["error"]);
                            }
                        }
                    
                        $('html, body').animate({
                            scrollTop: $(".controls").find(".error-msg").eq(0).offset().top - oTable_object.scroll_margin_top
                        }, 500);
                    }
                }
            });
        },
        del: function(url) {
            oTable_object = this;
            $.ajax({
                url: url,
                type: 'post',
                async: true,
                dataType: 'json',
                error: function(request) {
                    console.log(request.responseText);
                },
                success: function(json) {
                    if (json.status == 'success') {
                        oTable_object.refresh();
                    } else {
                        alert(json.msg);
                    }

                }
            });
        },
        pre_loading_otable_process: function() {
            //tab
            $(".tab-pane#action-tab").find(".control-group").removeClass("error");
            $(".tab-pane#action-tab").find(".control-group").removeClass("success");
            $(".tab-pane#action-tab").find(".control-group .error-msg").remove();
            $(".tab-pane#action-tab .form-actions").append(oTable_object.loading_animation_process);
            
            //modal
            $("#datatable-modal .modal-footer").prepend(oTable_object.loading_animation_process);
            $("#datatable-modal").find(".control-group").removeClass("error");
            $("#datatable-modal").find(".control-group").removeClass("success");
            $("#datatable-modal").find(".control-group .error-msg").remove();
            
            $("#btn-datatable-action").addClass("disabled");
        },
        post_loading_otable_process: function() {
            //tab
            $(".tab-pane#action-tab .form-actions").find(".loading-oTable-process").remove();
            
            //modal
            $("#datatable-modal .modal-footer").find(".loading-oTable-process").remove();
            
            $("#btn-datatable-action").removeClass("disabled");
        }

    };

}
;






jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function(oSettings, iDelay) {
    var _that = this;

    if (iDelay === undefined) {
        iDelay = 1000;
    }

    this.each(function(i) {
        $.fn.dataTableExt.iApiIndex = i;
        var
        $this = this,
        oTimerId = null,
        sPreviousSearch = null,
        anControl = $('input', _that.fnSettings().aanFeatures.f);

        anControl.unbind('keyup').bind('keyup', function() {
            var $$this = $this;

            if (sPreviousSearch === null || sPreviousSearch != anControl.val()) {
                window.clearTimeout(oTimerId);
                sPreviousSearch = anControl.val();
                oTimerId = window.setTimeout(function() {
                    $.fn.dataTableExt.iApiIndex = i;
                    _that.fnFilter(anControl.val());
                }, iDelay);
            }
        });

        return this;
    });
    return this;
};




jQuery.fn.dataTableExt.oApi.fnFilterOnButton = function(oSettings) {
    /*
     * Usage:       $('#example').dataTable().fnFilterOnButton();
     */
    var _that = this;

    this.each(function(i) {
        $.fn.dataTableExt.iApiIndex = i;
        var $this = this;
        var anControl = $('input', _that.fnSettings().aanFeatures.f);
        anControl.unbind('keyup');
        anControl.bind('keyup', function(event) {
            // KEYUP 'ENTER' EVENT
            if (event.keyCode == 13) {
                _that.fnFilter(anControl.val());
            }

            // ON EMPTY VALUE EVENT
            if (anControl.val() == '') {
                _that.fnFilter('');
            }
        });

        // BUTTON SEARCH CLICK EVENT
        var searchButton = $('#search-oTable').bind('click', function(e) {
            _that.fnFilter(anControl.val());
        });


        return this;
    });
    return this;
}


$(function() {
    oTable.init();
});