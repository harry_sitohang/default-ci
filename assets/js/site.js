var site = {
    init : function(){
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        }); 
				
        $('.scrollup').click(function(){
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
    },
    build_clean_url: function(url) {
        return url.replace(/&/g, "and").replace(/[^a-zA-Z0-9 _-]+/g, '').replace(/\s/gi, '-').replace(/--+/g, '-').toLowerCase();
    }
}

$(function(){
    site.init(); 
});