if(typeof(crud) === 'undefined'){
    crud = {
        fields : null,
        init : function(){
            crud_object = this;
            if((typeof(oTable) !== 'undefined') && oTable.fields != null){
                this.fields = oTable.fields;
            }else{
                this.load_fields();
                oTable.fields = this.fields;
            }
            
            //            for(var i=0; i < this.fields.length; i++){
            //                            if(this.fields[i].type.match(/^text$/)){
            //                                myTinyMCE.initTinyMCE({'elements' : this.fields[i].name});
            //                            }else if(this.fields[i].type.match(/^date$/)){
            //                                $( "#"+this.fields[i].name ).datepicker({
            //                                    dateFormat : 'yy-mm-dd',
            //                                    showButtonPanel: true
            //                                });
            //                            }
            //                            else if(this.fields[i].type.match(/^datetime$/)){
            //                                $( "#"+this.fields[i].name ).datetimepicker({
            //                                    dateFormat : 'yy-mm-dd',
            //                                    timeFormat: 'HH:mm:ss'
            //                                });
            //                            }
            //            }
            
          
            for (var field in this.fields) {
                if(this.fields[field].type.match(/^text$/i)){
                    myTinyMCE.initTinyMCE({
                        'elements' : this.fields[field].name
                    });
                }else if(this.fields[field].type.match(/^date$/i)){
                    $( "#"+this.fields[field].name ).datepicker({
                        dateFormat : 'yy-mm-dd',
                        showButtonPanel: true
                    });
                }
                else if(this.fields[field].type.match(/^datetime$/i)){
                    $( "#"+this.fields[field].name ).datetimepicker({
                        dateFormat : 'yy-mm-dd',
                        timeFormat: 'HH:mm:ss'
                    });
                }
            }
            
            this.addtional_init();
        
        },
        
        addtional_init : function(){
        //ADDITIONAL PROCESS IN INIT
        },
        
        
        load_fields : function(){
            crud_object = this;
            $.ajax({
                url : CLASS_URL+"get_fields_data/json",
                type : 'post',
                async : false,
                dataType : 'json',
                error : function(request){
                    console.log(request.responseText);
                },
                success : function(json){
                    //console.log(CLASS_URL+"get_fields_data/json"+" : crud.js "+ json);
                    crud_object.fields = json;
                }
            }); 
        }
    };
};


$(function(){
    crud.init();
//alert(JSON.stringify(myTinyMCE.options));
//console.log("location crud.js, field data : "+JSON.stringify(crud.fields));
});