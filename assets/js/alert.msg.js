alert_msg = {
    options : {
        type : null,
        dom_container : null,
        title : '',
        msg : '',
        fill_type : '',
        show_title : false
    },
    
    init : function(option){
        $.extend(this.options, option);
        //alert(this.options.show_title);
        this.show();
    },
    
    show : function(){
           
        var alert_class;
        switch(this.options.type){
            case 'warning':
                alert_class = 'alert';
                break;
            case 'success' :
                alert_class = 'alert alert-success';
                break;
            case 'error' :
                alert_class = 'alert alert-error';
                break;
            case 'info' :
                alert_class = 'alert alert-info';
                break;
            default :
                alert_class = 'alert alert-block';
                break;
        }
        
        var alert_title = (this.options.show_title == false) ? '' : '<strong>'+this.options.title+'</strong>';
        switch(this.options.fill_type){
            case 'append':
                if(this.options.dom_container.children('div.alert').length > 0){
                    this.options.dom_container.children('div.alert').remove();
                }
                this.options.dom_container.append('<div class="'+alert_class+'"><button type="button" class="close" data-dismiss="alert">&times;</button>'+alert_title+' '+this.options.msg+'</div>');
                break;
            case 'prepend' :
                if(this.options.dom_container.children('div.alert').length > 0){
                    this.options.dom_container.children('div.alert').remove();
                }
                this.options.dom_container.prepend('<div class="'+alert_class+'"><button type="button" class="close" data-dismiss="alert">&times;</button>'+alert_title+' '+this.options.msg+'</div>');
                break;
            case 'replace' :
                this.options.dom_container.html('<div class="'+alert_class+'"><button type="button" class="close" data-dismiss="alert">&times;</button>'+alert_title+' '+this.options.msg+'</div>');
                break;
            default :
                
                this.options.dom_container.html('<div class="'+alert_class+'"><button type="button" class="close" data-dismiss="alert">&times;</button>'+this.options.title+' '+this.options.msg+'</div>');
                break;
        }
    }
}