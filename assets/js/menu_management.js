var menu_management = function () {
    return {
        //main function to initiate the module
        init: function () {
            $(".select2_sample3").select2({
                tags: default_menu_action//["add", "edit", "delete", "detail"]
            });
                
            $(".select2-input").bind("keyup", function(e){
                e.stopPropagation();
            });
            
            $("#menu_url_segment").bind("keyup", function(e){
                $(this).val(site.build_clean_url($(this).val()));
            });
        }
    };

}();
    
jQuery(document).ready(function() {    
    menu_management.init();
});