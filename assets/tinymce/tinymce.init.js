var myTinyMCE = {
    tinyMCEObj : null,
    options : {
        //selector: "textarea",
        mode : "exact",
        relative_urls : false,
        convert_urls : false,
        elements : "rte",
        plugins : [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
        //"moxiemanager"
        ],
        //file_browser_callback : "ajaxfilemanager",
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        content_css : ASSETS_CSS+'all.css'
    },
    initTinyMCE : function(option){
       
        $.extend(this.options, option);
        //        if(typeof(oTable) !== 'undefined' && oTable.request_type.match(/^(ajax|tab)$/i)){
        //            this.options.theme_advanced_buttons1_add_before = "newdocument,fullscreen,print,separator";
        //        }
        //console.log(this.options);
        this.tinyMCEObj = tinyMCE.init(this.options);
    }
};


