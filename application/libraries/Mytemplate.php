<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/*
  | -------------------------------------------------------------------
  |  My template Libraries
  | -------------------------------------------------------------------
  | These are the classes located in the system/libraries folder
  | or in your application/libraries folder.
  |
  | to use this template load this automatic, configure ur application/config/autoload.php
  | $autoload['libraries'] = array('mytemplate');
  |
  | for autoload js, css make ur config 'template.php' file in application/config/template
  | prototype :
  |       $config['auto_css'] = array(
  |           'assets/css/common.css'
  |       );
  |
  |       $config['auto_js'] = array(
  |           'assets/js/jquery/jquery.js'
  |       );
  |
 */

class Mytemplate {

    var $template_view = "-";
    var $header_view = "-";
    var $menu_view = "-";
    var $footer_view = "-";
    var $main_view = "-";
    var $css = array();
    var $js = array();
    var $data = array();
    var $autoload_css = array();
    var $autoload_js = array();
    var $web_subtitle = "";
    var $load_css_script = "";
    var $load_js_script = "";
    var $template_used = "";

    function __construct($props = array()) {
        if (count($props) > 0) {
            $this->initialize($props);
        }
    }

    function initialize($config = array()) {
        $CI = & get_instance();
        $template_list = $CI->config->item("template_list");

        $defaults = array(
            'template_view' => '-',
            'header_view' => "-",
            'menu_view' => "-",
            'footer_view' => "-",
            'main_view' => '-',
            'data' => array(),
            'autoload_css' => $template_list[$config['template_used']]['auto_css'],
            'autoload_js' => $template_list[$config['template_used']]['auto_js'],
            'css' => array(),
            'js' => array(),
            'web_subtitle' => '',
            'template_used' => ''
        );


        foreach ($defaults as $key => $val) {
            if (isset($config[$key])) {
                $method = 'set_' . $key;
                if (method_exists($this, $method)) {
                    $this->$method($config[$key]);
                } else {
                    $this->$key = $config[$key];
                }
            } else {
                $this->$key = $val;
            }
        }
        unset($CI);
    }

    function set_template_view($param_template_view) {
        $this->template_view = $param_template_view;
    }

    function set_data($param_data = array()) {
        $this->data = array_merge($this->data, $param_data);
    }

    function get_data($data_key = '') {
        return !(empty($data_key)) ? $this->data[$data_key] : $this->data;
    }

    function add_css($param_autoload_css) {
        if (!empty($param_autoload_css)) {
            if (is_array($param_autoload_css)) {
                foreach ($param_autoload_css as $css) {
                    $this->autoload_css[] = $css;
                }
            } else {
                $this->autoload_css[] = $param_autoload_css;
            }
        }
    }

    function add_js($param_autoload_js) {
        if (!empty($param_autoload_js)) {
            if (is_array($param_autoload_js)) {
                foreach ($param_autoload_js as $js) {
                    $this->autoload_js[] = $js;
                }
            } else {
                $this->autoload_js[] = $param_autoload_js;
            }
        }
    }

    function set_header_view($param_header_view) {
        $this->header_view = $param_header_view;
    }

    function set_menu_view($param_menu_view) {
        $this->menu_view = $param_menu_view;
    }

    function set_main_view($param_main_view) {
        $this->main_view = $param_main_view;
    }

    function set_footer_view($param_footer_view) {
        $this->footer_view = $param_footer_view;
    }

    function set_web_subtitle($param_web_subtitle) {
        $this->web_subtitle = $param_web_subtitle;
    }

    function generate_css_script($arr_style = array()) {
        if (is_array($arr_style) && !empty($arr_style)) {
            foreach ($arr_style as $style) {
                if (substr($style, -4) == '.css')
                    $this->load_css_script .= '<link rel="stylesheet" type="text/css" media="all" href="' . base_url() . $style . '" />';
            }
        }
        else if (!is_array($arr_style)) {
            if (substr($arr_style, -4) == '.css')
                $this->load_css_script .= '<link rel="stylesheet" type="text/css" media="all" href="' . base_url() . $style . '" />';
        }
        else {
            $this->load_css_script .= "";
        }
    }

    function generate_js_script($arr_js = array()) {
        if (is_array($arr_js) && !empty($arr_js)) {
            foreach ($arr_js as $js) {
                if (substr($js, -3) == '.js')
                    $this->load_js_script .= '<script type="text/javascript" src="' . base_url() . $js . '"></script>';
            }
        }
        else if (!is_array($arr_js)) {
            if (substr($arr_js, -3) == '.js')
                $this->load_js_script .= '<script type="text/javascript" src="' . base_url() . $js . '"></script>';
        }
        else {
            $this->load_js_script .= "";
        }
    }

    function generate() {

        $this->generate_css_script($this->autoload_css);
        $this->generate_css_script($this->css);
        $this->generate_js_script($this->autoload_js);
        $this->generate_js_script($this->js);


        $this->load($this->template_view, array(
            'data' => $this->data,
            'my_style' => $this->load_css_script,
            'my_js' => $this->load_js_script,
            'header_view' => ($this->header_view != '-') ? $this->load($this->header_view, array(), TRUE) : "",
            'menu_view' => ($this->menu_view != '-') ? $this->load($this->menu_view, array(), TRUE) : "",
            'main_view' => ($this->main_view != '-') ? $this->load($this->main_view, array(), TRUE) : "",
            'footer_view' => ($this->footer_view != '-') ? $this->load($this->footer_view, array(), TRUE) : ""
        ));
    }

    function load($path_view, $data_view = array(), $return_string = FALSE) {
        $CI = & get_instance();
        if (is_array($data_view)) {
            $this->set_data($data_view);
        }
        if ($return_string === TRUE) {
            return $CI->load->view($path_view, $this->data, $return_string);
        } else {
            $CI->load->view($path_view, $this->data, $return_string);
        }
    }

    function load_use_path($path_view, $data_view = array(), $return_string = FALSE) {
        $CI = & get_instance();
        $path_view = "{$this->get_data("template_path")}{$path_view}";
        //echo $path_view; die;
        if (is_array($data_view)) {
            $this->set_data($data_view);
        }
        if ($return_string === TRUE) {
            return $CI->load->view($path_view, $this->data, $return_string);
        } else {
            $CI->load->view($path_view, $this->data, $return_string);
        }
    }

}

?>