<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @author Harry Osmar Sitohang
 * @desc : class master : divide controller_type "frontend" & "backend"
 * @date : 2 Jan 2014
 */
class Master extends CI_Controller {

    protected $controller_type;
    protected $views_path;
    protected $template_path;
    protected $class_name;
    protected $class_url;
    protected $assets_url;
    protected $assets_js;
    protected $assets_css;
    protected $base_url;
    protected $uri_string;
    protected $meta_data;
    protected $web_profile;

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : constructor controller master
     * @date : 2 Jan 2014
     */
    public function __construct($props = array()) {
        parent::__construct();
        if (count($props) > 0) {
            $this->initialize_master($props);
        }

        $this->initialize_view();
        $this->check_authorize();
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : initialize master controller, set 'controller_type' and 'view_path'
     * @date : 2 Jan 2014
     */
    protected function initialize_master($config) {
        $defaults = array(
            'controller_type' => '', //frontend|backend
        );

        foreach ($defaults as $key => $val) {
            $method = 'set_' . $key;
            if (method_exists($this, $method)) {
                $this->$method((isset($config[$key])) ? $config[$key] : $val);
            } else {
                $this->$key = (isset($config[$key])) ? $config[$key] : $val;
            }
        }

        $this->views_path = (preg_match('/^backend$/i', $this->controller_type)) ? $this->config->item("administrator_page_path") : $this->config->item("frontend_page_path");
        $this->template_path = (preg_match('/^backend$/i', $this->controller_type)) ? $this->config->item("template_path_backend") : $this->config->item("template_path_frontend");
        $this->class_name = strtolower(get_class($this));
        $this->assets_url = base_url('assets') . '/';
        $this->assets_js = base_url('assets/js') . '/';
        $this->assets_css = base_url('assets/css') . '/';
        $this->base_url = base_url() . ((index_page()) ? index_page() . '/' : '');
        $this->uri_string = uri_string();
        $this->meta_data = $this->get_page_meta_data();
        $this->web_profile = $this->get_web_profile_data();
        $this->class_url = base_url(((index_page()) ? index_page() . '/' : '') . $this->views_path . strtolower(get_class($this))) . '/';
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set 'controller_type', call in 'initialize_master()' function
     * @date : 2 Jan 2014
     */
    protected function set_controller_type($param_controller_type) {
        $this->controller_type = $param_controller_type;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : initialize template, set template-variable that will be used in all page, like class-varible, meta-tag, etc
     * @date : 2 Jan 2014
     */
    protected function initialize_view() {
        $template_used = $this->config->item("template_{$this->controller_type}");
        $template_list = $this->config->item("template_list");
        $this->mytemplate->initialize(array(
            'template_view' => $template_list[$template_used]["template_view"],
            'menu_view' => isset($template_list[$template_used]["menu_view"]) ? $template_list[$template_used]["menu_view"] : "-",
            'footer_view' => isset($template_list[$template_used]["footer_view"]) ? $template_list[$template_used]["footer_view"] : "-",
            'template_used' => $template_used
        ));

        $this->mytemplate->set_data(array(
            'class_name' => $this->class_name,
            'class_url' => $this->class_url,
            'assets_url' => $this->assets_url,
            'assets_js' => $this->assets_js,
            'assets_css' => $this->assets_css,
            'base_url' => $this->base_url,
            'views_path' => $this->views_path,
            'template_path' => $this->template_path,
            'controller_type' => $this->controller_type,
            'uri_string' => $this->uri_string,
            'meta_data' => $this->meta_data,
            'web_profile' => $this->web_profile
        ));
    }

    /**
     * @author Harry Osmar Sitohang
     * @return object
     * @desc : get meta data by uri-segment, this function called in 'get_page_meta_data()' function
     * @date : 2 Jan 2014
     */
    protected function get_page_meta_data() {
        $this->load->model('Meta_model');
        $meta_data = $this->Meta_model->get_where('*', array('uri_segment' => $this->uri_string))->row();
        if (empty($meta_data)) {
            $meta_data = $this->Meta_model->get_where('*', array('uri_segment' => ""))->row();
        }
        return $meta_data;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return object
     * @desc : get meta data by uri-segment, this function called in 'get_page_meta_data()' function
     * @date : 2 Jan 2014
     */
    protected function get_web_profile_data() {
        $this->load->model('Setting_model');
        return $this->Setting_model->get_where('*')->row();
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : check user authorize access
     * @date : 2 Jan 2014
     */
    protected function check_authorize() {
        if (preg_match('/^frontend|backend/i', $this->controller_type)) {
            if (preg_match('/^frontend$/i', $this->controller_type) && preg_match($this->config->item("user_auth_page"), $this->uri_string) && !$this->session->userdata('user_id')) {
                redirect('login?redirect=' . urlencode(current_url()));
            }

            if (preg_match('/^backend/i', $this->controller_type)) { // if not login yet
                if (!$this->session->userdata('user_id')) {
                    redirect('login?redirect=' . urlencode(current_url()));
                } else if (!preg_match('/^1$/i', $this->session->userdata('user_type'))) { // if not admin privilege
                    redirect(base_url());
                }
            }
        } else {
            die("'{$this->controller_type}' : is invalid controller type '/^frontend|backend$/i'");
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : login function, view login-form, process request, validate request-data, set session
     * @date : 2 Jan 2014
     */
    public function login() {
        require_once(APPPATH . 'libraries/recaptchalib.php'); //load recaptcha google library
        // check if user already login
        if ($this->session->userdata('user_id')) {
            $this->login_redirect($this->session->userdata('user_type'));
        }

        $this->load->library('form_validation');
        if ($this->input->post('action') && $this->input->post('action') == 'login') {
            $this->form_validation->set_error_delimiters('<p class="text-error">', '</p>');
            $this->load->model('User_model', 'user');

            //CEK AVALIABLE USER
            $row = $this->user->get_where('*', array(
                        'email' => $this->input->post('email'),
                        'password' => sha1($this->config->item('encrypt_salt') . $this->input->post('password'))
                    ))->row();

            //set user row to POST , for later used by 'user_check' callback function
            $_POST['user_data'] = $row;

            $web_profile = $this->mytemplate->get_data('web_profile');
            $form_validation_config = (preg_match('/^YES$/i', $web_profile->use_recaptcha)) ? 'login' : 'login_wot_recaptcha';
            if ($this->form_validation->run($form_validation_config)) {
                //if successfully login set session
                $this->session->set_userdata(array(
                    'user_id' => $row->user_id,
                    'user_email' => $row->email,
                    'user_type' => $row->type
                ));

                //UPDATE TABLE LOGIN
                $this->db->update('users', array(
                    'last_login' => date('Y-m-d H:i:s'),
                    'last_ip' => $this->input->ip_address()), array('user_id' => $row->user_id)
                );

                //echo $this->db->last_query(); die;
                $this->login_redirect($row->type);
            } else {
                //first unset 'action' POST variable before calling recursive function 'login', to resolve infinite looping 
                unset($_POST['action']);
                $this->login();
            }
        } else {
            $this->mytemplate->set_main_view("master/login");
            $this->mytemplate->generate();
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : redirect after login success
     * @date : 2 Jan 2014
     */
    public function login_redirect($user_type) {
        if (($_GET["redirect"]) && !empty($_GET["redirect"])) {
            redirect($_GET["redirect"]);
        } else if (preg_match('/^2$/i', $user_type)) {
            redirect(base_url()); // user type member => redirect to home
        } else {
            redirect($this->config->item("administrator_page_path") . 'meta'); // user type member => redirect to order
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : logout function, unset session variable
     * @date : 2 Jan 2014
     */
    public function logout() {
        $this->session->unset_userdata(array(
            'user_id' => '',
            'user_email' => '',
            'user_type' => ''
        ));
        redirect('login');
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : RESET PASSSWORD
     * @date : 2 Jan 2014
     */
    public function reset_password() {
        if (!$this->session->userdata('user_id')) {
            //redirect to login
            redirect('login');
        }

        $this->load->library('form_validation');

        $user_id = preg_match('/^frontend$/i', $this->controller_type) ? $this->session->userdata('user_id') : $this->uri->segment(4);


        $this->load->model('User_model', 'user');
        $row_user = $this->user->get_by_id($user_id)->row();

        if (empty($row_user)) {
            $redirect = preg_match('/^frontend$/i', $this->controller_type) ? 'reset-password' : $this->views_path . lcfirst(get_class($this));
            redirect($redirect);
        }


        if ($this->input->post('action') && $this->input->post('action') == "reset_password") {
            $this->form_validation->set_error_delimiters('<p class="text-error">', '</p>');


            if ($this->form_validation->run("reset-password-{$this->controller_type}")) {
                //if successfully update password
                $this->db->update('users', array('password' => sha1($this->config->item('encrypt_salt') . $this->input->post('new_password'))), array('user_id' => $user_id));

                $redirect = preg_match('/^frontend$/i', $this->controller_type) ? 'reset-password' : $this->views_path . lcfirst(get_class($this));
                redirect($redirect);
            } else {
                //first unset 'action' POST variable before calling recursive function 'login', to resolve infinite looping 
                unset($_POST['action']);
                $this->reset_password();
            }
        } else {
            $this->mytemplate->set_data(array(
                'user_id' => $user_id,
                'controller_type' => $this->controller_type
            ));
            $this->mytemplate->set_main_view("master/reset_password");
            $this->mytemplate->generate();
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return boolean
     * @desc : callback-form-validation "login" check user data, is exist in database
     * @date : 2 Jan 2014
     */
    public function user_check() {
        $user_data = $this->input->post('user_data');
        if ($this->input->post('user_data') && !empty($user_data)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('user_check', 'Maaf data anda tidak ditemukan');
            return FALSE;
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return boolean
     * @desc : callback-form-validation "login" check user data, is user active
     * @date : 2 Jan 2014
     */
    public function user_is_activated() {
        $user_data = $this->input->post('user_data');
        if (preg_match('/^YES$/i', $user_data->activated)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('user_is_activated', 'Maaf akun anda belum diaktifasi oleh admin');
            return FALSE;
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return boolean
     * @desc : callback-form-validation "login" check user data, is user is banned
     * @date : 2 Jan 2014
     */
    public function user_is_banned() {
        $user_data = $this->input->post('user_data');
        if (preg_match('/^NO$/i', $user_data->banned)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('user_is_banned', 'Maaf akun anda diblok oleh admin, karena alasan sbb : ' . $user_data->ban_reason);
            return FALSE;
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return boolean
     * @desc : callback-form-validation "login" check google-recaptcha
     * @date : 2 Jan 2014
     */
    public function check_recaptcha() {
        $web_profile = $this->mytemplate->get_data('web_profile');

        $resp = recaptcha_check_answer($web_profile->recaptcha_private_key, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        if (!$resp->is_valid) {
            // What happens when the CAPTCHA was entered incorrectly
            $this->form_validation->set_message('check_recaptcha', "The reCAPTCHA wasn't entered correctly. Go back and try it again." .
                    "(reCAPTCHA said: " . $resp->error . ")");
            return FALSE;
        } else {
            // Your code here to handle a successful verification
            return TRUE;
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return array
     * @desc : upload image
     * @date : 2 Jan 2014
     */
    function uploadImage($field_name = 'datafile', $upload_config = array()) {
        $this->load->library('upload');
        $this->upload->initialize($upload_config);
        return $this->upload->do_upload($field_name);
    }

    /**
     * @author Harry Osmar Sitohang
     * @return boolean
     * @desc : resize original image
     * @date : 2 Jan 2014
     */
    function resizeOriImage($image_source_path = '', $width = '100', $height = '100') {
        $this->load->library('image_lib');
        $this->image_lib->initialize(array(
            'image_library' => 'gd2',
            'source_image' => $image_source_path,
            'maintain_ratio' => TRUE,
            'width' => $width,
            'height' => $height
        ));
        return $this->image_lib->resize();
    }

    /**
     * @author Harry Osmar Sitohang
     * @return boolean
     * @desc : resize image & create thumbs
     * @date : 2 Jan 2014
     */
    function resizeImage($image_source_path = '', $width = '100', $height = '100', $thumb_marker = '_thumb') {
        $this->load->library('image_lib');
        $this->image_lib->initialize(array(
            'image_library' => 'gd2',
            'source_image' => $image_source_path,
            'create_thumb' => TRUE,
            'thumb_marker' => $thumb_marker,
            'maintain_ratio' => TRUE,
            'width' => $width,
            'height' => $height
        ));
        return $this->image_lib->resize();
    }

}

/* End of file master.php */
/* Location: ./application/core/controllers/master.php */