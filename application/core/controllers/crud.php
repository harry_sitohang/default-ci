<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/controllers/master' . EXT;

/**
 * @author Harry Osmar Sitohang
 * @desc : class crud : provide CRUD function
 * @date : 2 Jan 2014
 * @extend : master
 */
class Crud extends Master {

    public $my_model;
    public $class_name;
    private $my_model_fields;
    public $request_type;
    public $fields_data;
    public $file_fields = array();
    public $unset_fields = array();
    public $primary_field;
    public $history_attribute = array(
        "field_creator" => "creator_user_id",
        "field_created_date" => "created_date",
        "field_updater" => "last_update_user_id",
        "field_last_updated_date" => "last_updated_date"
    );

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : constructor
     * @date : 2 Jan 2014
     */
    public function __construct($props = array()) {

        parent::__construct($props);

        if (count($props) > 0) {
            $this->initialize_crud($props);
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : initialize this datatable attribute class
     * @date : 2 Jan 2014
     */
    public function initialize_crud($config = array()) {

        $defaults = array(
            'my_model' => '',
            'class_name' => '',
            'request_type' => 'normal' //'tab', 'modal', 'normal'
        );

        foreach ($defaults as $key => $val) {
            $method = 'set_' . $key;
            if (method_exists($this, $method)) {
                $this->$method((isset($config[$key])) ? $config[$key] : $val);
            } else {
                $this->$key = (isset($config[$key])) ? $config[$key] : $val;
            }
        }

        $this->set_my_model_fields();
        $this->set_fields_data();
        $this->set_primary_field();
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set model CRUD : by construstor parameter
     * @date : 2 Jan 2014
     */
    public function set_my_model($param_my_model) {
        $this->my_model = $param_my_model;
        $this->load->model($this->my_model);
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set current class name : by construstor parameter
     * @date : 2 Jan 2014
     */
    public function set_class_name($param_class_name) {
        $this->class_name = lcfirst($param_class_name);
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set fields get from model
     * @date : 2 Jan 2014
     */
    public function set_my_model_fields() {
        $this->my_model_fields = $this->{$this->my_model}->get_fields();
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set field data from model, then set variable "fields_data" to template
     * @date : 2 Jan 2014
     */
    public function set_fields_data() {
        $merge_fields = $this->fields_data_custom($this->{$this->my_model}->get_fields_data());
        if (is_array($merge_fields) && !empty($merge_fields)) {
            foreach ($merge_fields as $key => $val) {
                $merge_fields[$key] = (Object) $val;
            }
        }

        $this->fields_data = array_merge($this->{$this->my_model}->get_fields_data(), $merge_fields);
        //echo "<pre>"; print_r($this->fields_data); die;
        $this->mytemplate->set_data(array('fields_data' => $this->fields_data, 'unset_fields' => $this->unset_fields));
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set PK field from model
     * @date : 2 Jan 2014
     */
    public function set_primary_field() {
        foreach ($this->fields_data as $field_name => $field) {
            if ($field->primary_key == 1) {
                $this->primary_field = $field_name;
                break;
            }
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return string
     * @desc : get current class_name
     * @date : 2 Jan 2014
     */
    public function get_class_name() {
        return $this->class_name;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return array
     * @desc : set CRUD VALIDATION RULES for all fields
     * @date : 2 Jan 2014
     */
    public function validation_rules($action) {

        $fields = $this->fields_data;
        $config = array();
        foreach ($fields as $field) {
            $unset = isset($field->unset) ? $field->unset : "NO";

            if (preg_match("/^NO$/i", $unset)) {
                //echo $this->{$this->my_model}->PK; echo $action; echo $field;  die;
                if ($action == 'add' && $field->name == $this->{$this->my_model}->PK) {
                    continue;
                }

                $pre_conditions_rules = "trim|xss_clean|strip_tags";
                $required = (isset($field->required) && preg_match("/^NO$/i", $field->required)) ? "" : "required";
                if (preg_match('/^(text|rte)$/i', $field->type)) {
                    $rules = "{$required}";
                } else if (preg_match('/^int$/i', $field->type)) {
                    $required = "|{$required}";
                    $rules = "{$pre_conditions_rules}{$required}|integer";
                } else if (preg_match('/^(datetime|timestamp)$/i', $field->type)) {
                    $required = "|{$required}";
                    $rules = preg_match("/^YES$/i", $required) ? "{$pre_conditions_rules}{$required}|callback_datetime_check" : "{$pre_conditions_rules}{$required}";
                } else if (preg_match('/^date$/i', $field->type)) {
                    $required = "|{$required}";
                    $rules = preg_match("/^YES$/i", $required) ? "{$pre_conditions_rules}{$required}|callback_date_check" : "{$pre_conditions_rules}{$required}";
                } else {
                    $required = "|{$required}";
                    $rules = "{$pre_conditions_rules}{$required}";
                }

                $rules .= isset($field->max_length) ? "|max_length[{$field->max_length}]" : "";
                $rules .= isset($field->min_length) ? "|min_length[{$field->min_length}]" : "";

                $rules .= (isset($field->is_unique) && preg_match("/^YES$/i", $field->is_unique)) ? "|callback_check_is_unique_field[{$field->name}]" : "";
                
                $config[$field->name] = array(
                    'field' => $field->name,
                    'label' => ucwords(preg_replace('/[_]/', ' ', $field->name)),
                    'rules' => $rules
                );
            }
        }

        $file_config = $this->file_validation($action);
        $config = array_merge($config, $file_config);
        //print_r($config);die;
        //$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|max_length[12]|is_unique[users.username]');
        return $config;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return array
     * @desc : set validation for field-type "file"
     * @date : 2 Jan 2014
     */
    public function file_validation($action) {
        $file_config = array();
        if (((preg_match('/^edit$/i', $action) && $this->input->post('change_image')) || (preg_match('/^add$/i', $action))) && !$this->input->is_ajax_request()) {
            foreach ($this->file_fields as $field) {
                $file_config[$field] = array(
                    'field' => "{$field}",
                    'label' => ucwords(preg_replace('/[_]/', ' ', $field)),
                    'rules' => "callback_check_image"
                );
            }
        } else {
//            foreach ($this->file_fields as $field) {
//                $file_config[$field] = array(
//                    'field' => "{$field}",
//                    'label' => ucwords(preg_replace('/[_]/', ' ', $field)),
//                    'rules' => "xss_clean"
//                );
//            }
            foreach ($this->file_fields as $field) {
                foreach ($this->fields_data[$field]->resize as $resize_field => $resize_data) {
                    $file_config[$resize_field] = array(
                        'field' => "{$resize_field}",
                        'label' => ucwords(preg_replace('/[_]/', ' ', $resize_field)),
                        'rules' => "xss_clean"
                    );
                }
            }
        }
        return $file_config;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return json
     * @desc : set validation for field-type "file", this call by ajax
     * @date : 2 Jan 2014
     */
    public function check_image_ajax() {
        if ($this->input->is_ajax_request()) {
            //echo '<pre>'; print_r($_FILES); die;
            //print_r($this->file_fields); die;
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<label class="error-msg">', '</label>');

            foreach ($this->file_fields as $field) {
                $_POST["trigger_check_image"] = TRUE;
                $this->form_validation->set_rules($field, ucwords(preg_replace('/[_]/', ' ', $field)), 'callback_check_image');
            }

            if ($this->form_validation->run() === TRUE) {
                echo json_encode(array('status' => 'success', 'files' => $_POST));
            } else {
                echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
            }
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return booelan
     * @desc : callback "check_image", this callback process will upload image 
     * @date : 2 Jan 2014
     */
    public function check_image() {
        //echo '<pre>'; print_r($this->file_fields); die;
        //echo '<pre>'; print_r($_FILES); die;

        unset($_POST['trigger_check_image']);

        foreach ($this->file_fields as $field) {
            //echo print_r($this->fields_data[$field]); die;
            //upload image
            if ($this->uploadImage("{$field}", $this->fields_data[$field]->upload)) {
                $upload_data = $this->upload->data();
                $resize_config = $this->fields_data[$field]->resize;


                foreach ($resize_config as $file_field_name => $resize_data) {
                    $resize_response = ($resize_data['resize_ori_img'] === TRUE) ? $this->resizeOriImage($upload_data['full_path'], $resize_data['w'], $resize_data['h']) : $this->resizeImage($upload_data['full_path'], $resize_data['w'], $resize_data['h'], $resize_data['sufiks_filename']);
                    if ($resize_response) {
                        $_POST["{$file_field_name}"] = $upload_data['raw_name'] . $resize_data['sufiks_filename'] . $upload_data['file_ext'];
                    } else {
                        $this->form_validation->set_message('check_image', $this->image_lib->display_errors());
                        return FALSE;
                    }
                }
            } else {
                $this->form_validation->set_message('check_image', $this->upload->display_errors());
                return FALSE;
            }
        }

        return TRUE;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return array
     * @desc : customize your field data
     * @date : 11 Mar 2014
     */
    public function fields_data_custom($field_data) {
        return $this->unset_input_field_form_crud($field_data);
        //return array(); //ex : //"image":{"name":"name","type":"varchar","default":null,"max_length":"500","primary_key":0}
    }

    /**
     * @author Harry Osmar Sitohang
     * @return array/json
     * @desc : GET FIELDS DATA
     * @date : 2 Jan 2014
     */
    public function get_fields_data($type = 'array') {
        if ($type == 'json') {//if ($this->input->is_ajax_request()) {
            echo json_encode($this->fields_data);
        } else {
            //print_r($this->{$this->my_model}->get_fields_data()); die;
            return $this->fields_data;
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : SET TAB LI TITLE, OVERRIDE THIS FUNCTION TO CHANGE TAB TITLE
     * @date : 2 Jan 2014
     */
    public function set_tab_li_title($row) {
        //$row = (Array) $row;
        foreach ($row as $val) {
            return $val;
            break;
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void/json
     * @desc : check is valid row-ID
     * @date : 2 Jan 2014
     */
    public function is_valid_row($id) {
        $valid_row = $this->{$this->my_model}->get_by_id($id)->row();
        if (empty($valid_row)) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array('status' => 'error', 'msg' => 'Invalid Row ID'));
                die;
            } else {
                redirect("{$this->views_path}{$this->class_name}");
            }
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return @void
     * @desc : set CRUD DETAIL VIEW
     * @date : 2 Jan 2014
     */
    public function detail($id) {
        $this->is_valid_row($id);

        $this->mytemplate->set_main_view('crud/detail');
        $row = $this->{$this->my_model}->get_by_id($id, '*')->row();
        if (empty($row)) {
            redirect($this->class_name);
        }
        $this->mytemplate->set_data(array(
            'row' => $row
        ));

        $this->mytemplate->generate();
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set CRUD EDIT VIEW AND ACTION
     * @date : 2 Jan 2014
     */
    public function edit($id) {
        $this->is_valid_row($id);

        $this->load->library('form_validation');
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_error_delimiters('<label class="error-msg">', '</label>');
        } else {
            $this->form_validation->set_error_delimiters('<label class="error-msg label-inline">', '</label>');
        }

        //$fields = $this->get_fields_data();
        $fields = $this->customize_field($this->fields_data, 'edit');

        if ($this->input->post('action') && $this->input->post('action') == 'edit') {
            //echo '<pre>'; print_r($this->validation_rules('edit')); die;
            $this->set_post_data_history_attribute('edit', $fields);

            $this->form_validation->set_rules($this->validation_rules($this->input->post('action')));
            if ($this->form_validation->run() == TRUE) {
                $data = array();
                foreach ($fields as $field) {
                    if (isset($_POST[$field->name])) {//if ($this->input->post($field->name)) {
                        $data[$field->name] = $this->input->post($field->name);
                    }
                }

                $data = $this->customize_data_before_crud($data);

                if ($this->db->update($this->{$this->my_model}->table, $data, array($this->{$this->my_model}->PK => $id))) {
                    //echo $this->db->last_query(); die;
                    if ($this->input->is_ajax_request()) {
                        if ((!$this->input->post('change_image'))) {
                            echo json_encode(array('status' => 'success', 'msg' => "successfully {$this->input->post('action')} your data"));
                        } else {
                            $return_image = array();
                            foreach ($this->file_fields as $field_name) {
                                $url_upload = $this->fields_data[$field_name]->url_upload;
                                $return_images[$field_name] = $url_upload . '/' . $_POST[$this->fields_data[$field_name]->return_image];
                            }
                            echo json_encode(array('status' => 'success', 'msg' => "successfully {$this->input->post('action')} your data", 'return_images' => $return_images));
                        }
                    } else {
                        $this->session->set_flashdata('status', 'success');
                        redirect("{$this->views_path}{$this->class_name}/edit/$id");
                    }
                } else {
                    if ($this->input->is_ajax_request()) {
                        echo json_encode(array('status' => 'error', 'msg' => mysql_error()));
                    }
                }
            } else {
                //echo validation_errors(); die;
                if ($this->input->is_ajax_request()) {
                    if ($this->input->post('change_image')) {
                        $this->delete_old_files_ajax();
                    }
                    echo json_encode(array('status' => 'error', 'msg' => validation_errors(), "fields" => $this->set_form_error_fields()));
                } else {
                    unset($_POST['action']); //UNSET action POST VARIABLE BEFORE SENDING IT BACK TO VIEW, TO HANDLE LOOPING FOREVER
                    $this->edit($id); //SENDING BACK TO VIEW
                }
            }
        } else {
            //SET ENUM AND RELATION DATA
            $enum = array();
            $relation = array();
            foreach ($fields as $field) {
                if (preg_match('/^enum$/i', $field->type)) {
                    $enum[$field->name] = $this->{$this->my_model}->get_enum_values($field->name);
                } else if (isset($field->relation)) {
                    $relation = explode('|', $field->relation);
                    $this->db->select(array($relation[1], $relation[2]));
                    $this->db->from($relation[0]);
                    $relation[$field->name] = create_form_dropdown_options($this->db->get()->result_array(), $relation[1], $relation[2]);
                }
            }

            if ($this->input->is_ajax_request()) {
                $this->set_editor_view_ajax('edit', $fields, $enum, $relation, $id);
            } else {
                $this->set_editor_view('edit', $fields, $enum, $relation, $id);
                $this->mytemplate->generate();
            }
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set CRUD ADD VIEW AND ACTION
     * @date : 2 Jan 2014
     */
    public function add() {
        //echo '<pre>'; print_r($_FILES); die;
        $this->load->library('form_validation');
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_error_delimiters('<label class="error-msg">', '</label>');
        } else {
            $this->form_validation->set_error_delimiters('<label class="error-msg label-inline">', '</label>');
        }

        //$fields = $this->get_fields_data();
        //$fields = $this->customize_field($fields, 'add');
        $fields = $this->customize_field($this->fields_data, 'add');

        if ($this->input->post('action') && $this->input->post('action') == 'add') {
            //echo '<pre>'; print_r($this->validation_rules()); die;

            $this->set_post_data_history_attribute('add', $fields);

            $this->form_validation->set_rules($this->validation_rules($this->input->post('action')));
            if ($this->form_validation->run() == TRUE) {
                $data = array();
                foreach ($fields as $field) {
                    if ($this->input->post($field->name)) {
                        $data[$field->name] = $this->input->post($field->name);
                    }
                }
                $data = $this->customize_data_before_crud($data);
                //echo '<pre>'; print_r($_POST); die;
                if ($this->db->insert($this->{$this->my_model}->table, $data)) {
                    if ($this->input->is_ajax_request()) {
                        echo json_encode(array('status' => 'success', 'msg' => "successfully {$this->input->post('action')} your data"));
                    } else {
                        $this->session->set_flashdata('status', 'success');
                        redirect("{$this->views_path}{$this->class_name}/add");
                    }
                } else {
                    if ($this->input->is_ajax_request()) {
                        echo json_encode(array('status' => 'error', 'msg' => mysql_error()));
                    }
                }
            } else {
                if ($this->input->is_ajax_request()) {
                    $this->delete_old_files_ajax();
                    echo json_encode(array('status' => 'error', 'msg' => validation_errors(), "fields" => $this->set_form_error_fields()));
                } else {
                    unset($_POST['action']); //UNSET action POST VARIABLE BEFORE SENDING IT BACK TO VIEW, TO HANDLE LOOPING FOREVER
                    $this->add(); //SENDING BACK TO VIEW
                }
            }
        } else {
            //SET ENUM AND RELATION DATA
            $enum = array();
            $relation = array();
            foreach ($fields as $field) {
                if (preg_match('/^enum$/i', $field->type)) {
                    $enum[$field->name] = $this->{$this->my_model}->get_enum_values($field->name);
                } else if (preg_match('/^relation$/i', $field->type)) {//(isset($field->relation)) {
                    $relation_query_segment = explode('|', $field->relation);
                    $this->db->select(array($relation_query_segment[1], $relation_query_segment[2]));
                    $this->db->from($relation_query_segment[0]);
                    $relation[$field->name] = create_form_dropdown_options($this->db->get()->result_array(), $relation_query_segment[1], $relation_query_segment[2]);
                }
            }

            if ($this->input->is_ajax_request()) {
                $this->set_editor_view_ajax('add', $fields, $enum, $relation);
            } else {
                $this->set_editor_view('add', $fields, $enum, $relation);
                $this->mytemplate->generate();
            }
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc set POST attribute history for action edit & add
     */
    public function set_post_data_history_attribute($action, $fields) {
        if (preg_match("/^add$/i", $action)) { //only for add action
            if (isset($fields[$this->history_attribute["field_creator"]]))
                $_POST[$this->history_attribute["field_creator"]] = $this->session->userdata("user_id");
            if (isset($fields[$this->history_attribute["field_created_date"]]))
                $_POST[$this->history_attribute["field_created_date"]] = date("Y-m-d H:i:s");
        }

        //for edit and add action
        if (isset($fields[$this->history_attribute["field_updater"]]))
            $_POST[$this->history_attribute["field_updater"]] = $this->session->userdata("user_id");
        if (isset($fields[$this->history_attribute["field_last_updated_date"]]))
            $_POST[$this->history_attribute["field_last_updated_date"]] = date("Y-m-d H:i:s");
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc unset field input form
     */
    public function unset_input_field_form_crud($field_data) {
        foreach ($this->history_attribute as $field_name) {
            if (isset($field_data[$field_name]))
                $field_data[$field_name]->unset = "YES";
        }
        return $field_data;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : delete row
     * @date : 2 Jan 2014
     */
    public function delete($id) {
        $this->is_valid_row($id);

        if (!empty($this->file_fields)) {
            $this->delete_old_files($id);
        }

        if ($this->db->delete($this->{$this->my_model}->table, array($this->{$this->my_model}->PK => $id))) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array('status' => 'success', 'msg' => 'successfully delete your data'));
            } else {
                redirect("{$this->views_path}{$this->class_name}");
            }
        } else {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array('status' => 'error', 'msg' => 'eror when trying to delete your data ' . mysql_error()));
            } else {
                redirect("{$this->views_path}{$this->class_name}");
            }
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return array
     * @desc : override this to customize data before CRUD action
     * @param array POST : $data
     * @date : 2 Jan 2014
     */
    public function customize_data_before_crud($data) {
        if ((preg_match('/^edit$/i', $this->input->post('action')) && $this->input->post('change_image'))) {
            $this->delete_old_files($this->input->post("{$this->primary_field}"));
        }
        return $data;
    }

    /**
     * @desc customize field berfore view
     * @override 
     */

    /**
     * @author Harry Osmar Sitohang
     * @return array
     * @desc : override this to  customize per row in CRUD datatable
     * @param fields & action for row
     * @date : 2 Jan 2014
     */
    public function customize_field($fields, $action) {
        return $fields;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set editor view, override this if want to use your own ADD/EDIT form
     * @date : 2 Jan 2014, 
     * last edit 3 Jan 2014 add parameter 'request_type' => $this->request_type, edit "crud/edit_body"
     */
    public function set_editor_view($action, $fields, $enum, $relation, $id = '') {
        $this->mytemplate->set_main_view('crud/edit');
        $this->mytemplate->set_data(array(
            'fields' => $fields,
            'fields_data' => $this->fields_data,
            'pk' => $this->{$this->my_model}->PK,
            'table' => ucwords(preg_replace('/[_]/', ' ', $this->{$this->my_model}->table)),
            'row' => (!empty($id)) ? $this->{$this->my_model}->get_by_id($id, '*')->row() : array(),
            'enum' => $enum,
            'relation' => $relation,
            'action' => $action,
            'multipart' => (!empty($this->file_fields)) ? TRUE : FALSE,
            'pattern_unset_fields' => '/^(' . implode('|', $this->unset_fields) . ')$/i',
            'request_type' => $this->request_type
        ));
    }

    /**
     * @author Harry Osmar Sitohang
     * @return json
     * @desc : set editor view called by ajax request, override this if want to use your own ADD/EDIT form
     * @date : 2 Jan 2014
     * last edit 3 Jan 2014 add parameter 'request_type' => $this->request_type, edit "crud/edit_body"
     */
    public function set_editor_view_ajax($action, $fields, $enum, $relation, $id = '') {
        //print_r($this->file_fields); die;
        $row = $this->{$this->my_model}->get_by_id($id, '*')->row();
        $data = array(
            'modal_body' => $this->mytemplate->load('crud/edit_body', array(
                'fields' => $fields,
                'pk' => $this->{$this->my_model}->PK,
                'table' => ucwords(preg_replace('/[_]/', ' ', $this->{$this->my_model}->table)),
                'row' => $row,
                'enum' => $enum,
                'relation' => $relation,
                'action' => $action,
                'multipart' => (!empty($this->file_fields)) ? TRUE : FALSE,
                'pattern_unset_fields' => '/^(' . implode('|', $this->unset_fields) . ')$/i',
                'request_type' => $this->request_type
                    ), TRUE),
            'modal_header_title' => ucwords(preg_replace('/[_]/', ' ', $this->{$this->my_model}->table)),
            'action' => $action,
            'request_type' => $this->request_type
        );

        if (preg_match('/^tab$/i', $this->request_type)) {
            if (preg_match('/^edit$/i', $action)) {
                $data['tab_li_title'] = $this->set_tab_li_title($row);
                $data['id'] = $id;
                $data['modal_action'] = $this->mytemplate->load('crud/action', array('action' => 'edit', 'id' => $id), TRUE);
            } else {
                $data['modal_action'] = $this->mytemplate->load('crud/action', array('action' => 'add'), TRUE);
            }
        }

        echo json_encode($data);
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : delete old file, for upload image by id ROW
     * @date : 2 Jan 2014
     */
    public function delete_old_files($id) {
        //DELETE OLD IMAGES
        $row = $this->{$this->my_model}->get_by_id($id, '*')->row();
        foreach ($this->file_fields as $field) {
            $upload_config = $this->fields_data[$field]->upload;
            $resize_config = $this->fields_data[$field]->resize;
            foreach ($resize_config as $field_resize_name => $resize_data) {
                if (file_exists($upload_config['upload_path'] . $row->{$field_resize_name}) && !is_dir($upload_config['upload_path'] . $row->{$field_resize_name})) {
                    unlink($upload_config['upload_path'] . $row->{$field_resize_name});
                }
            }
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : set error form per field
     * @date : 3 Fed 2014
     */
    public function set_form_error_fields() {
        $fields = array();
        foreach ($this->get_fields_data() as $key => $field) {
            $fields[$key] = $field;
            $fields[$key]->error = form_error($key);
        }
        return $fields;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : delete old file, for upload image by id ROW : called by ajax request
     * @date : 2 Jan 2014
     */
    public function delete_old_files_ajax() {
        foreach ($this->file_fields as $field) {
            $resize_config = $this->fields_data[$field]->resize;
            $upload_path = $this->fields_data[$field]->upload['upload_path'];

            foreach ($resize_config as $file_field_name => $resize_data) {
                if (file_exists($upload_path . $this->input->post($file_field_name)) && !is_dir($upload_path . $this->input->post($file_field_name))) {
                    unlink($upload_path . $this->input->post($file_field_name));
                }
            }
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return boolean
     * @desc : CALLBACK FORM VALIDATION 'DATETIME-CHECK'
     * @date : 2 Jan 2014
     */
    public function datetime_check($date) {
        if (preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/', $date)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('datetime_check', 'The %s field must be a valid datetime');
            return FALSE;
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return boolean
     * @desc : CALLBACK FORM VALIDATION 'DATE-CHECK'
     * @date : 2 Jan 2014
     */
    public function date_check($date) {
        if (preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $date)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('date_check', 'The %s field must be a valid date');
            return FALSE;
        }
    }
    
    /**
     * @author Harry Osmar Sitohang
     * @return boolean
     * @desc : CALLBACK check unique field value
     * @date : 11 Maret 2014
     */
    public function check_is_unique_field($val, $field_name) {
        $this->db->select("COUNT(1) AS `count`");
        $this->db->from($this->{$this->my_model}->table);
        $this->db->where($field_name, $val);
        if (preg_match("/^edit$/i", $this->input->post("action"))) {
            $this->db->where("{$this->{$this->my_model}->PK} <> ", $this->input->post($this->{$this->my_model}->PK));
        }
        $count = $this->db->get()->row()->count;
        //echo $this->db->last_query(); die;
        if ($count == 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_is_unique_field', 'The %s field must be unique');
            return FALSE;
        }
    }

}

/* End of file crud.php */
/* Location: ./application/core/controllers/crud.php */