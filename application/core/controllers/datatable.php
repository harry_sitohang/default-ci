<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/controllers/crud' . EXT;

/**
 * @author Harry Osmar Sitohang
 * @desc : class datatable
 * @date : 2 Jan 2014
 * @extend : crud
 */
class Datatable extends Crud {

    public $fields = array();
    //public $my_model;
    public $class_name;
    public $number_counter;
    public $column_action;
    public $show_pk = FALSE;
    public $hidden_fields = array();
    private $bSortable = array();
    private $bVisible = array();
    private $bSearchable = array();

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : constructor
     * @date : 2 Jan 2014
     */
    public function __construct($props = array()) {

        parent::__construct($props);

        $this->mytemplate->add_css('assets/DataTables/media/css/jquery.dataTables.css');
        $this->mytemplate->add_css('assets/DataTables/media/css/DT_bootstrap.css');
        $this->mytemplate->add_css('assets/jquery-ui/time-picker/jquery-ui-timepicker-addon.css');
        $this->mytemplate->add_css('assets/tinymce/tinymce_upload.css');
        $this->mytemplate->add_css('assets/jquery-ui/themes/ui-lightness/jquery-ui.css');
        
        $this->mytemplate->add_js('assets/DataTables/media/js/jquery.dataTables.js');
        $this->mytemplate->add_js('assets/DataTables/media/js/DT_bootstrap.js');
        $this->mytemplate->add_js('assets/jquery-ui/time-picker/jquery-ui-timepicker-addon.js');
        $this->mytemplate->add_js('assets/jquery-ui/time-picker/jquery-ui-sliderAccess.js');
        $this->mytemplate->add_js('assets/tinymce/tinymce.min.js');
        $this->mytemplate->add_js('assets/tinymce/tinymce.init.js');
        $this->mytemplate->add_js('assets/tinymce/tinymce_upload.js');
        $this->mytemplate->add_js('assets/js/oTable.js');
        
        //$this->initialize_view();
        if (count($props) > 0) {
            $this->initialize_datatable($props);
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return void
     * @desc : generate index page : override this index if necessary, for example if you want to have your own table-format
     * @date : 2 Jan 2014
     */
    public function index() {

        $this->mytemplate->set_main_view('datatables/table');
        $this->mytemplate->set_data(array(
            'request_type' => $this->request_type,
            'number_counter' => $this->number_counter,
            'column_action' => $this->column_action
        ));

        $this->mytemplate->set_data(array(
            'columns_header' => $this->set_column_header($this->fields)
        ));

        $this->mytemplate->generate();
    }

    /**
     * @author Harry Osmar Sitohang
     * @return string
     * @desc : generate header column  : override this function to customize your 'header column' view
     * @date : 2 Jan 2014
     */
    public function set_column_header($fields) {
        return $this->mytemplate->load('datatables/heads', array('fields' => $fields), TRUE);
    }

    /**
     * @author Harry Osmar Sitohang
     * @return @void
     * @desc : initialize this datatable attribute class
     * @date : 2 Jan 2014
     */
    function initialize_datatable($config = array()) {


        $defaults = array(
            'my_model' => '',
            'class_name' => '',
            'hidden_fields' => array(),
            'number_counter' => TRUE,
            'column_action' => TRUE,
            'hidden_fields' => array(),
            'fields' => array(),
            'show_pk' => FALSE
        );

        foreach ($defaults as $key => $val) {
            $method = 'set_' . $key;
            if (method_exists($this, $method)) {
                $this->$method((isset($config[$key])) ? $config[$key] : $val);
            } else {
                $this->$key = (isset($config[$key])) ? $config[$key] : $val;
            }
        }

        /*
         * SET COLUMN DEFENITION AFTER ALL CONFIG VARIABLE,
         * BECAUSE THIS FUNCTION NEED TO CHECK CONFIG VARIABLE VALUE SUCH AS 
         * 'show_pk', 'fields', 'hidden_fields', 'column_action', 'number_counter'
         */

        $this->set_aoColumnDefs();
    }

    /**
     * @author Harry Osmar Sitohang
     * @return @void
     * @desc : set fields, get field from db compare with array field parameter, for PK will always be used
     * @date : 2 Jan 2014
     */
    function set_fields($param_fields = '') {
        if (!empty($param_fields) && is_array($param_fields)) {
            //AUTOMATICALLY ADD PRIMARY KEY FOR DEFAULT IS HIDDEN => $this->show_pk = FALSE
            array_unshift($param_fields, $this->{$this->my_model}->PK);
            $this->fields = $param_fields;
        } else {
            $this->fields = $this->{$this->my_model}->get_fields();
        }
    }

    /**
     * @desc initialize view template
     * @void
     */
//    protected function initialize_view() {
//        //parent::initialize_view();
//
//        $this->mytemplate->add_css('assets/DataTables/media/css/jquery.dataTables.css');
//        $this->mytemplate->add_css('assets/DataTables/media/css/DT_bootstrap.css');
//        $this->mytemplate->add_js('assets/DataTables/media/js/jquery.dataTables.js');
//        $this->mytemplate->add_js('assets/DataTables/media/js/DT_bootstrap.js');
//    }

    /**
     * @author Harry Osmar Sitohang
     * @return json : contained all result for datatable in current view 
     * @desc : he core datatable proses
     * @date : 2 Jan 2014
     */
    function dt_server_processing($return_type = 'json') {
        $data = array();
        $data['aColumns'] = $this->fields;
        $data['sIndexColumn'] = $this->{$this->my_model}->get_PK();

        /**
         * @Paging
         */
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $data['limit'] = $_GET['iDisplayLength'];
            $data['offset'] = $_GET['iDisplayStart'];
        }

        /**
         * @Ordering
         */
        $data['order'] = array();
        if (isset($_GET['iSortCol_0'])) {
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $data['order'][] = $data['aColumns'][intval($_GET['iSortCol_' . $i]) - (($this->number_counter === TRUE) ? 1 : 0)] . ' ' . $_GET['sSortDir_' . $i];
                }
            }
        }

        $data['order'] = (!empty($data['order'])) ? implode(', ', $data['order']) : "";

        //print_r($data['order']);  die;

        /**
         * @Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $data['sWhere'] = array();
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            for ($i = 0; $i < count($data['aColumns']); $i++) {
                $data['sWhere'][$data['aColumns'][$i]] = $_GET['sSearch'];
            }
        }

        $table_data = $this->{$this->my_model}->dt_server_processing($data);

        $number = isset($data['offset']) ? $data['offset'] : 0; //COUNTER NUMBER
        //SET FIELD FOR EACH ROW, LOOPING RESULT
        foreach ($table_data['rResult'] as $aRow) {
            $number++;
            $table_data['output']['aaData'][] = $this->set_row($data, $aRow, $number, $return_type);
        }

        //FOR DEBUGGING
        $table_data['output']['query'] = $this->db->last_query();

        if (preg_match('/^json$/i', $return_type)) {
            echo json_encode($table_data['output']);
        } else if (preg_match('/^xls$/i', $return_type)) {
            $this->report($table_data['output']['aaData']);
        } else {
            echo json_encode($table_data['output']);
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return array
     * @desc : set per row for DISPLAYED IN datatable override this function to customize 'view of your row'
     * @date : 2 Jan 2014
     */
    function set_row($data, $aRow, $number, $return_type = 'json') {
        $this->load->helper('text');
        $row = array();

        //IF NUMBER COUNTER SET TO TRUE 
        if (preg_match('/^json$/i', $return_type) && $this->number_counter === TRUE) {
            $row[] = $number;
        }

        //print_r($data['aColumns']); return false;
        //print_r($this->fields_data); return false;

        foreach ($data['aColumns'] as $aColumn) {
            if (isset($this->fields_data[$aColumn]) && isset($this->fields_data[$aColumn]->relation)) {
                $relation = explode('|', $this->fields_data[$aColumn]->relation);
                $this->db->select($relation[2]);
                $this->db->from($relation[0]);
                $this->db->where($relation[1], $aRow->$aColumn);
                $value = $this->db->get()->row()->$relation[2];
                //echo $this->db->last_query(); die;
            } else if (isset($this->fields_data[$aColumn]) && preg_match('/^(date|datetime)$/i', $this->fields_data[$aColumn]->type)) {
                $value = format_date($aRow->$aColumn);
            } else if (isset($this->fields_data[$aColumn]) && preg_match('/^(text)$/i', $this->fields_data[$aColumn]->type)) {
                $value = word_limiter(strip_tags($aRow->$aColumn), 25);
            } else if (isset($this->fields_data[$aColumn]) && preg_match('/^(file)$/i', $this->fields_data[$aColumn]->type)) {
                $value = '<img class="img-rounded" src=' . $this->fields_data[$aColumn]->url_upload . '/' . $aRow->{$this->fields_data[$aColumn]->return_image} . '>';
            } else {
                $value = $aRow->$aColumn;
            }

            if (preg_match('/^json$/i', $return_type)) {
                $row[] = $value;
            } else {
                $row[$aColumn] = $value;
            }

            if (!preg_match('/^json$/i', $return_type) && in_array($aColumn, $this->unset_fields)) {
                unset($row[$aColumn]);
            }
        }

//        if (preg_match('/^fk_category_product_id$/i', $aColumn)) {
//                $this->load->model('Category_product_model', 'cpm');
//                $row[] = $this->cpm->get_by_id($aRow->$aColumn, 'product_category_title')->row()->product_category_title;
//            } 
        //IF COLUMN ACTION === TRUE
        if (preg_match('/^json$/i', $return_type) && $this->column_action === TRUE) {
            $row[] = $this->set_action_row($aRow);
        }

        return $row;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return string
     * @desc : set action per row in datatable override this function to customize 'action for each of your row'
     * @date : 2 Jan 2014
     */
    function set_action_row($aRow) {
        //echo $this->class_name; die;
        return $this->load->view('datatables/dt_action', array('row' => $aRow, 'class_url' => $this->class_url, 'PK' => $this->{$this->my_model}->PK), TRUE);
    }

    /**
     * @author Harry Osmar Sitohang
     * @return JSON
     * @desc : get ajax dt-config in JSON format call from ajax to initialize datatable 
     * @date : 2 Jan 2014
     */
    function dt_config_template($config = array()) {
        if ($this->input->is_ajax_request()) {
            echo json_encode($this->initialize_dt_config($config));
        }
    }

    /**
     * @author Harry Osmar Sitohang
     * @return array
     * @desc : set column defenition like 'sortable', 'visible', 'searchable'
     * @date : 2 Jan 2014
     */
    function set_aoColumnDefs() {
        $bSortable = array();
        $bVisible = array();
        $bSearchable = array();

        //SETTING COLUMN DEFENITION FOR 'COUNTER NUMBER COLUMN'
        if ($this->number_counter === TRUE) {
            $bSortable[] = 0;
            $bSearchable[] = 0;
        }


        foreach ($this->fields as $key => $val) {
            if (($val == $this->{$this->my_model}->PK && $this->show_pk === FALSE) || in_array($val, $this->hidden_fields)) {
                $bSortable[] = ($this->number_counter === TRUE) ? $key + 1 : $key;
                $bVisible[] = ($this->number_counter === TRUE) ? $key + 1 : $key;
                $bSearchable[] = ($this->number_counter === TRUE) ? $key + 1 : $key;
            }
        }

        //SETTING COLUMN DEFENITION FOR 'ACTION COLUMN'
        if ($this->column_action === TRUE) {
            $bSortable[] = ($this->number_counter === TRUE) ? $key + 2 : $key + 1;
            $this->bSearchable = ($this->column_action === TRUE && $this->number_counter === TRUE) ? $key + 2 : $key + 1;
        }

        $this->bSortable = $bSortable;
        $this->bVisible = $bVisible;
        $this->bSearchable = $bSearchable;
    }

    /**
     * @author Harry Osmar Sitohang
     * @return array array-config-datatable
     * @desc : return default value of datatable config to change this default value please override this function in child class
     * @date : 2 Jan 2014
     */
    function initialize_dt_config($config = array(), $_additional_parameter = array()) {

        foreach ($this->unset_fields as $unset_field) {
            $val = ($this->number_counter === TRUE) ? intval(array_search($unset_field, $this->fields)) + 1 : intval(array_search($unset_field, $this->fields));
            $this->bSortable[] = $val;
            $this->bVisible[] = $val;
            $this->bSearchable[] = $val;
        }


        $defaults = array(
            "sDom" => '<"top"lf>rt<"bottom"ip><"clear">',
            "bProcessing" => true,
            "bServerSide" => true,
            "sAjaxSource" => "{$this->class_url}dt_server_processing",
            "aaSorting" => array(
                array(0, 'asc')
            ),
            "sScrollX" => "100%",
            "sScrollXInner" => "100%",
            "bScrollCollapse" => true,
            "sPaginationType" => "bootstrap", //"full_numbers",
            "aoColumnDefs" => array(
                array(
                    "bSortable" => false,
                    "aTargets" => $this->bSortable
                ),
                array(
                    "bVisible" => false,
                    "aTargets" => $this->bVisible
                ),
                array(
                    "bSearchable" => false,
                    "aTargets" => $this->bSearchable
                )
            ),
            'bStateSave' => true,
            'iCookieDuration' => 3600,
            "oLanguage" => array(//how entries option
                "sLengthMenu" => $this->load->view('datatables/s_length_menu', '', TRUE)
            ),
            "iDisplayLength" => 10
        );


        foreach ($defaults as $key => $val) {
            if (isset($config[$key])) {
                $defaults[$key] = $config[$key];
            }
        }

        $additional_parameter = array_merge(array('request_type' => $this->request_type), $_additional_parameter);

        return array(
            'dt_config' => $defaults,
            'additional_parameter' => $additional_parameter
        );
    }

    /**
     * @author Harry Osmar Sitohang
     * @return MIME TYPE XLS
     * @desc : generate report in XLS
     * @date : 2 Jan 2014
     */
    public function report($result) {
        //echo '<pre>';print_r($result);die;
        error_reporting(E_ALL);

        //load our new PHPExcel library
        $this->load->library('excel');

        // Set document properties
        $this->excel->getProperties()->setCreator("raenz.com")
                ->setLastModifiedBy("raenz.com")
                ->setTitle(preg_replace('/[_]/i', ' ', $this->class_name) . " XLS Report Document")
                ->setSubject(preg_replace('/[_]/i', ' ', $this->class_name) . " XLS Report Document")
                ->setDescription(preg_replace('/[_]/i', ' ', $this->class_name) . " for XLS")
                ->setKeywords("office XLS openxml php")
                ->setCategory(preg_replace('/[_]/i', ' ', $this->class_name));

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);

        //name the worksheet
        $this->excel->getActiveSheet()->setTitle(preg_replace('/[_]/i', ' ', $this->class_name));

        //set row header
        $head = array_keys(current($result));
        foreach ($head as $key => $val) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($key, 1, preg_replace('/[_]/i', ' ', $val));
        }


        $baris = 2;
        foreach ($result as $row) {
            foreach ($head as $key => $val) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($key, $baris, $row[$val]);
            }
            $baris++;
        }


        $filename = preg_replace('/[_]/i', ' ', $this->class_name) . '.xls'; //save our workbook as this file name
        header('Content-Type: application/octet-stream');
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Transfer-Encoding: binary');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

}

/* End of file datatable.php */
/* Location: ./application/core/controllers/datatable.php */