<!-- Modal -->
<div id="tinymce-media-dialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="myModalLabel"><i class="icon-film" style="margin-top: 7px;"></i>&nbsp;Media</h3>
    </div>
    <div class="modal-body">
        Loading...
    </div>
    <div class="modal-footer">
        <div class="btn-group pull-left">
            <button class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li id="check-uncheck-all-tinymce-media-img" is_checked="no"><a href="#"><i class="icon-check"></i>&nbsp;check all</a></li>
                <li id="delete-selected-tinymce-media-img"><a href="#"><i class="icon-trash"></i>&nbsp;delete selected</a></li>
                <li id="insert-selected-tinymce-media-img"><a href="#"><i class="icon-share"></i>&nbsp;insert selected</a></li>
            </ul>
        </div>

        <div class="pull-right">
            <img src="<?php echo "{$assets_url}img/loading.gif"; ?>" class="loading_tinymce_upload" style="display: none;">
            <form id="tinymce_form_upload" style="display: inline !important;" action="" method="post" enctype="multipart/form-data">
                <input name="tinymce_image" type="file">
            </form>
        </div>
    </div>
</div>