<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/css/login.css" />
<div class="login-box">
    <form class="form-signin" action="<?php //echo "{$class_url}/reset_password/{$this->uri->segment(4)}";  ?>" method="POST">
        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
        <h2 class="form-signin-heading">Silahkan isi input</h2>
        <input type="password" name="old_password" class="input-block-level" placeholder="<?php echo preg_match('/^backend/i', $controller_type) ? 'Admin Password' : 'Old Password'; ?>" value="<?php echo set_value('old_password'); ?>">
        <?php echo form_error('old_password'); ?>
        <input type="password" name="new_password" class="input-block-level" placeholder="New Password" value="<?php echo set_value('new_password'); ?>">
        <?php echo form_error('new_password'); ?>
        <input type="password" name="confirm_password" class="input-block-level" placeholder="Confirm New Password" value="<?php echo set_value('confirm_password'); ?>">
        <?php echo form_error('confirm_password'); ?>
        <!--<label class="checkbox">
            <input type="checkbox" value="remember-me"> Remember me
        </label>-->
        <input type="hidden" name="action" value="reset_password">
        <button class="btn btn-large btn-primary">Reset</button>
        <?php if (!preg_match('/^frontend$/i', $controller_type)): ?>
            <a href="<?php echo $class_url; ?>" class="btn btn-large">Cancel</a>
        <?php endif; ?>
    </form>
</div>
