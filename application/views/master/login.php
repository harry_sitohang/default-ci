<!--<script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>-->
<!--<script type="text/javascript">
    $(function(){
        Recaptcha.create("6LcaVtwSAAAAAF49OT-h1BJP__o033ZUftesT55W",
        "captchadiv",
        {
            tabindex: 3,
            theme: "red",
            callback: Recaptcha.focus_response_field
        }
    ); 
    });
</script>-->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $assets_css; ?>login.css" />
<div class="login-box">
    <form class="form-signin" action="" method="POST">
        <h2 class="form-signin-heading">Silahkan login</h2>
        <input type="text" name="email" class="input-block-level" placeholder="Email address" value="<?php echo set_value('email'); ?>">
        <?php echo form_error('email'); ?>
        <input type="password" name="password" class="input-block-level" placeholder="Password" value="<?php echo set_value('password'); ?>">
        <?php echo form_error('password'); ?>
        <!--<label class="checkbox">
            <input type="checkbox" value="remember-me"> Remember me
        </label>-->
        <!--        <div id="captchadiv"></div>-->
        <?php if (preg_match('/^YES$/i', $web_profile->use_recaptcha)): ?>
            <?php echo recaptcha_get_html($web_profile->recaptcha_public_key); ?>
            <?php echo form_error('recaptcha_challenge_field'); ?>
        <?php endif; ?>
        <input type="hidden" name="action" value="login">
        <button class="btn btn-large btn-primary">Sign in</button>
    </form>
</div>
