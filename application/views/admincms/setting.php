<div class="container">

    <form enctype="multipart/form-data" accept-charset="utf-8" method="POST" action="<?php echo $class_url; ?>">

        <div class="page-header">
            <h3>Website Setting</h3>
        </div>

        <div class="form-horizontal">

            <?php if ($this->session->flashdata('status') && $this->session->flashdata('status') == 'success'): ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">&times;</button> successfully edit your data
                </div>
            <?php endif; ?>

            <input type="hidden" value="edit" name="action">

            <?php foreach ($fields as $field): ?>
                <div class="control-group">
                    <label class="control-label" for="<?php echo $field->name; ?>"><?php echo preg_replace('/[_]/', ' ', $field->name); ?></label>
                    <div class="controls">  
                        <?php if (preg_match('/^enum$/i', $field->type)): ?>
                            <?php echo form_dropdown($field->name, $enum[$field->name], $data->{$field->name}); ?>
                        <?php elseif (preg_match('/^shortcut_icon$/i', $field->name)): ?>
                            <?php echo form_upload(array('name' => 'shortcut_icon', 'class' => 'span3')); ?>
                            <label class="checkbox">
                                <input type="checkbox" name="change_icon_image">
                                Ganti gambar
                            </label>
                            <img class="img-rounded" src="<?php echo $this->config->item('url_upload_shortcut_icon').$data->{$field->name}; ?>">
                        <?php else: ?>
                            <input type="text" name="<?php echo $field->name; ?>" value="<?php echo $data->{$field->name} ?>" class="input span3" placeholder="<?php echo preg_replace('/[_]/', ' ', $field->name); ?>">
                        <?php endif; ?>
                        <?php echo form_error($field->name); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <!--SUBMIT BUTTON-->
        <div class="form-actions">
            <button type="submit" class="btn btn-primary" id="btn-datatable-action" action="edit">Save changes</button>
            <a href="<?php echo $class_url; ?>" class="btn">Cancel</a>
        </div>



    </form>
</div>


<?php
//echo '<pre>';
//print_r($fields);
//print_r($data);
?>