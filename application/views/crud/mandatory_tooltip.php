<?php if (!(isset($field->required) && preg_match("/^NO$/i", $field->required) && $field->primary_key == FALSE)): ?>
    <span class="input-warning tooltips" data-original-title="<?php echo $field->name; ?> is required">
        <i class="icon-warning-sign"></i>
    </span>
<?php endif; ?>