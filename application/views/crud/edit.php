<div class="container">
    <?php echo ($multipart === TRUE) ? form_open_multipart() : form_open(); ?>

    <div class="page-header">
        <h3><?php echo ucwords($action); ?>&nbsp;<?php echo $table; ?></h3>
    </div>

    <?php $this->mytemplate->load('crud/edit_body', isset($row) ? array('row' => $row) : array() ); ?>


    <!--SUBMIT BUTTON-->
    <?php $this->load->view('crud/action', array('action' => $action, 'class_url' => $class_url)); ?>

    <?php echo form_close(); ?>
</div>