<div class="form-actions">
    <button type="submit" class="btn btn-primary blue" id="btn-datatable-action" action="<?php echo $action; ?>"><i class="icon-ok"></i>&nbsp;<?php echo ($action == 'edit') ? 'Save changes' : 'Add'; ?></button>
    <a href="<?php echo $class_url; ?>" class="btn">Cancel</a>
</div>