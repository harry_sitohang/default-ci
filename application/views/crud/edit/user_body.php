<div class="row">
    <div class="span11">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-user"></i>Data User</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="scroller" style="height:100px" data-always-visible="1" data-rail-visible="0">
                    <form action="#" class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">
                                <input type="email" name="email" id="email" placeholder="Email" class="m-wrap medium" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Type</label>
                            <div class="controls">
                                <?php $field = $fields_data['type']; ?>
                                <?php echo form_dropdown($field->name, $relation[$field->name], ($this->input->post($field->name)) ? $this->input->post($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : ''), 'id="' . $field->name . '" class="m-wrap medium" style="' . ((isset($field->style)) ? $field->style : '') . '"'); ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="span11">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-lock"></i>Password</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="scroller" style="height:100px" data-always-visible="1" data-rail-visible="0">
                    <form action="#" class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">Password</label>
                            <div class="controls">
                                <input type="password" name="password" id="password" placeholder="Password" class="m-wrap medium" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Confirm Password</label>
                            <div class="controls">
                                <input type="password" name="confirm-password" id="confirm-password" placeholder="Confirm Password" class="m-wrap medium" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
//echo '<pre>'; print_r($enum);
//echo '<pre>'; print_r($row); 
//echo '<pre>'; print_r($fields_data);
//echo '<pre>'; print_r($_SERVER);
//echo '<pre>'; print_r($relation);
//echo '<pre>'; print_r($this->session->all_userdata());
?>