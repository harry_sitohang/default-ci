<!--<script src="<?php echo base_url(); ?>assets/js/crud.js" type="text/javascript"></script>-->

<div class="form-horizontal">

    <!--ALERT MESSAGE BOX-->
    <div id="msg-box">
        <?php if ($this->session->flashdata('status') && $this->session->flashdata('status') == 'success'): ?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert" type="button">&times;</button>
                Successfully <?php echo $action; ?> your data
            </div>
        <?php endif; ?>
    </div>

    <?php echo preg_match("/^normal$/i", $request_type) ? "" : (($multipart === TRUE) ? form_open_multipart() : form_open()); ?>

    <!--ACTION INPUT HIDDEN-->
    <input name="action" type="hidden" value="<?php echo $action; ?>">
    <input name="multipart" type="hidden" value=" <?php echo $multipart; ?>">




    <?php foreach ($fields as $field): ?>
        <!--check is mandatory-->
        <?php //$mandatory_tooltip = $this->mytemplate->load('crud/mandatory_tooltip', array("field" => $field), TRUE); ?>
        <!--Field customization from $fields_data parameter-->
        <?php $field->default = preg_match("/^timestamp$/i", $field->type) ? null : $field->default; ?>
        <?php $field->class = isset($field->class) ? $field->class : 'span3'; ?>
        <?php $field->style = isset($field->style) ? $field->style : ''; ?>
        <?php $field->plugin = isset($field->plugin) ? $field->plugin : ""; ?>
        <?php $field->unset = isset($field->unset) ? $field->unset : "NO"; ?>
        <?php $field->label = isset($field->label) ? $field->label : ucwords(preg_replace('/[_]/', ' ', $field->name)); ?>
        <?php $field->max_length_input_attr = isset($field->plugin) && preg_match("/char_count/i", $field->plugin) ? "max_length={$field->max_length}" : ""; ?>
        <?php $field->min_length_input_attr = isset($field->plugin) && preg_match("/char_count/i", $field->plugin) ? "min_length={$field->min_length}" : ""; ?>
        <?php $field->plugin = isset($field->plugin) ? $field->plugin : ""; ?>



        <?php if (preg_match("/^NO$/i", $field->unset)): ?>
            <!--ROW FIELD INPUT-->
            <div class="control-group">

                <!--LABEL FIELD-->
                <!--FOR FIELD PRIMARY-KEY DOESNT HAVE LABEL-->
                <?php if ($field->name != $pk): ?>
                    <label class="control-label" for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label>
                <?php endif; ?>



                <!--INPUT FIELD-->
                <div class="controls <?php //echo !empty($mandatory_tooltip) ? "input-icon" : "";             ?>">
                    <?php if ($field->name == $pk): ?>
                        <!--FOR FIELD PRIMARY-KEY TYPE HIDDEN-->
                        <?php if (isset($row->{$field->name})): ?>
                            <input class="<?php echo $field->class; ?>" plugin="<?php echo $field->plugin; ?>" <?php echo $field->max_length_input_attr; ?> <?php echo $field->min_length_input_attr; ?> style="<?php echo $field->style; ?>" field="pk" type="hidden" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>"  placeholder="<?php echo ucwords(preg_replace('/[_]/', ' ', $field->name)); ?>" value="<?php echo (set_value($field->name)) ? set_value($field->name) : $row->{$field->name}; ?>">
                        <?php endif; ?>
                    <?php elseif ((preg_match('/^(parent_id)$/i', $field->name))): ?>   
                         <?php echo $menu_dropdown; ?>
                    <?php elseif ((preg_match('/^(menu_action)$/i', $field->name))): ?>   
                        <input type="hidden" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>" class="span9 select2_sample3" value="<?php echo (set_value($field->name)) ? set_value($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : $field->default); ?>">
                    <?php elseif ((preg_match('/^(text|rte)$/i', $field->type))): ?>
                        <!--FOR FIELD-TYPE TEXT USED TINYMCE-->
                        <textarea class="<?php echo $field->class; ?>" plugin="<?php echo $field->plugin; ?>" <?php echo $field->max_length_input_attr; ?> <?php echo $field->min_length_input_attr; ?> style="<?php echo $field->style; ?>" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>"  placeholder="<?php echo ucwords(preg_replace('/[_]/', ' ', $field->name)); ?>"><?php echo (set_value($field->name)) ? set_value($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : $field->default); ?></textarea>
                    <?php elseif (preg_match('/^(relation)$/i', $field->type)): ?>
                        <!--FOR FIELD-TYPE RELATION USING  DROPDOWN-->
                        <?php echo form_dropdown($field->name, $relation[$field->name], ($this->input->post($field->name)) ? $this->input->post($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : ''), 'id="' . $field->name . '" class="span3" style="' . $field->style . '"'); ?>
                    <?php elseif (preg_match('/^(enum)$/i', $field->type)): ?>
                        <!--FOR FIELD-TYPE ENUMERATION USING  DROPDOWN-->
                        <?php echo form_dropdown($field->name, $enum[$field->name], ($this->input->post($field->name)) ? $this->input->post($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : ''), 'id="' . $field->name . '" class="span3" style="' . $field->style . '"'); ?>
                    <?php elseif (preg_match('/^(file)$/i', $field->type)): ?>
                        <input class="<?php echo $field->class; ?>" style="<?php echo $field->style; ?>" type="file" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
                        <?php if (preg_match('/^edit$/i', $action)): ?>
                            <label class="checkbox">
                                <input type="checkbox" image="<?php echo $field->name; ?>" name="change_image" <?php echo ($this->input->post('change_image')) ? 'checked="TRUE"' : ''; ?>>&nbsp;Ganti gambar
                            </label>
                            <?php if (isset($row->{$field->name . '_thumbs'})): ?>
                                <img class="img-rounded" type="image" image="<?php echo $field->name; ?>" src="<?php echo $fields_data[$field->name]->url_upload . '/' . $row->{$field->name . '_thumbs'}; ?>">
                            <?php endif; ?>                       
                        <?php endif; ?>
                    <?php elseif (preg_match('/^(password)$/i', $field->type)): ?>
                        <input class="<?php echo $field->class; ?>" plugin="<?php echo $field->plugin; ?>" <?php echo $field->max_length_input_attr; ?> <?php echo $field->min_length_input_attr; ?> style="<?php echo $field->style; ?>" type="password" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>"  placeholder="<?php echo ucwords(preg_replace('/[_]/', ' ', $field->name)); ?>" value="<?php echo (set_value($field->name)) ? set_value($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : $field->default); ?>">
                    <?php else: ?>
                        <!--OTHER FIELDS-->
                        <input class="<?php echo $field->class; ?>" plugin="<?php echo $field->plugin; ?>"  <?php echo $field->max_length_input_attr; ?> <?php echo $field->min_length_input_attr; ?> style="<?php echo $field->style; ?>" type="text" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>"  placeholder="<?php echo ucwords(preg_replace('/[_]/', ' ', $field->name)); ?>" value="<?php echo (set_value($field->name)) ? set_value($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : $field->default); ?>">
                    <?php endif; ?>



                    <?php //echo $mandatory_tooltip; ?>
                    <?php echo form_error($field->name); ?>
                </div>    


            </div>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php echo preg_match("/^normal$/i", $request_type) ? "" : form_close(); ?>
</div>

<script type="text/javascript">
    var default_menu_action = <?php echo json_encode(explode(',', $fields_data["menu_action"]->default)); ?>;
</script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/select2/select2_metro.css" rel="stylesheet" type="text/css" media="screen"/>
<script type="text/javascript" src="<?php echo "{$assets_url}metronic/"; ?>plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo "{$assets_url}/"; ?>js/menu_management.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->  


<?php
//echo '<pre>'; print_r($enum);
//echo '<pre>'; print_r($row); 
//echo '<pre>'; print_r($fields_data);
//echo '<pre>'; print_r($_SERVER);
//echo '<pre>'; print_r($relation);
//echo '<pre>'; print_r($this->session->all_userdata());
?>