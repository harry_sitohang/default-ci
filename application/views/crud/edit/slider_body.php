<script src="<?php echo base_url(); ?>assets/js/crud.js" type="text/javascript"></script>
<script type="text/javascript">
    $.extend(crud, {
        addtional_init : function(){
            //ADDITIONAL PROCESS IN INIT
            $("select[name=image_used]").live('change', function(){
                if($(this).val().match(/^URL$/i)){
                    $("input[type=file][name=slider_image]").attr('disabled', true);
                    $("input[type=checkbox][name=change_slider_image]").attr('checked', false);
                    $("input[type=checkbox][name=change_slider_image]").attr('disabled', true);
                    $("input[type=text][name=image_url]").attr('disabled', false);
                    $("input[type=text][name=image_url_thumbs]").attr('disabled', false);
                    $("select[name=delete_image_file]").attr('disabled', false); 
                }else{
                    $("input[type=file][name=slider_image]").attr('disabled', false);
                    $("input[type=checkbox][name=change_slider_image]").attr('disabled', false);
                    $("input[type=text][name=image_url]").attr('disabled', true);
                    $("input[type=text][name=image_url_thumbs]").attr('disabled', true);
                    $("select[name=delete_image_file]").attr('disabled', true); 
                    $("select[name=delete_image_file]").val('NO'); 
                }
            });
            
            $("select[name=is_a_link]").live('change', function(){
                if($(this).val().match(/^NO$/i)){
                    $("input[type=text][name=link_url]").attr('disabled', true);
                }else{
                    $("input[type=text][name=link_url]").attr('disabled', false);
                }
            });
            
            $("input[type=text][name=image_url]").live('blur', function(){
                $(this).parent().find('img').remove();
                $(this).parent().find('br').remove();
                $(this).parent().append('<br/><img src="'+$(this).val()+'" class="img-rounded" style="margin-top:15px;">');
            });
            
            $("input[type=text][name=image_url_thumbs]").live('blur', function(){
                $(this).parent().find('img').remove();
                $(this).parent().find('br').remove();
                $(this).parent().append('<br/><img src="'+$(this).val()+'" class="img-rounded" style="margin-top:15px;">');
            });
             
            $("select[name=image_used]").trigger('change');
            $("select[name=is_a_link]").trigger('change');
            $("input[type=text][name=image_url]").trigger('blur');
            $("input[type=text][name=image_url_thumbs]").trigger('blur');
        }
    });
    
    $(function(){
        crud.init();
    });
</script>


<div class="form-horizontal">


    <!--ALERT MESSAGE BOX-->
    <div id="msg-box">
        <?php if ($this->session->flashdata('status') && $this->session->flashdata('status') == 'success'): ?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert" type="button">&times;</button>
                Successfully <?php echo $action; ?> your data
            </div>
        <?php endif; ?>
    </div>

    <?php echo ($multipart === TRUE) ? form_open_multipart() : form_open(); ?>
    <!--ACTION INPUT HIDDEN-->
    <input name="action" type="hidden" value="<?php echo $action; ?>">

    <?php foreach ($fields as $field): ?>
        <!--ROW FIELD INPUT-->
        <div class="control-group">
            <!--LABEL FIELD-->
            <!--FOR FIELD PRIMARY-KEY DOESNT HAVE LABEL-->
            <?php if ($field->name == 'fk_category_slider_id'): ?>
                <label class="control-label" for="<?php echo $field->name; ?>">Kategori</label>
            <?php elseif ($field->name != $pk): ?>
                <label class="control-label" for="<?php echo $field->name; ?>"><?php echo ucwords(preg_replace('/[_]/', ' ', $field->name)); ?></label>
            <?php endif; ?>

            <!--INPUT FIELD-->
            <div class="controls">
                <?php if ($field->name == $pk): ?>
                    <!--FOR FIELD PRIMARY-KEY TYPE HIDDEN-->
                    <?php if (isset($row->{$field->name})): ?>
                        <input class="span3" field="pk" type="hidden" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>"  placeholder="<?php echo ucwords(preg_replace('/[_]/', ' ', $field->name)); ?>" value="<?php echo (set_value($field->name)) ? set_value($field->name) : $row->{$field->name}; ?>">
                    <?php endif; ?>
                <?php elseif ($field->type == 'text'): ?>
                    <!--FOR FIELD-TYPE TEXT USED TINYMCE-->
                    <textarea class="span3" rows="15" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>"  placeholder="<?php echo ucwords(preg_replace('/[_]/', ' ', $field->name)); ?>"><?php echo (set_value($field->name)) ? set_value($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : ''); ?></textarea>
                <?php elseif ($field->type == 'enum'): ?>
                    <!--FOR FIELD-TYPE ENUMERATION USING  DROPDOWN-->
                    <?php echo form_dropdown($field->name, $enum[$field->name], ($this->input->post($field->name)) ? $this->input->post($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : ''), 'id="' . $field->name . '" class="span3"'); ?>
                <?php elseif ($field->name == 'slider_image'): ?>
                    <input class="span3" type="file" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
                    <?php if (preg_match('/^edit$/i', $action)): ?>
                        <label class="checkbox">
                            <input type="checkbox" name="change_slider_image" <?php echo ($this->input->post('change_slider_image')) ? 'checked="TRUE"' : ''; ?>>&nbsp;Ganti gambar
                        </label>
                        <img class="img-rounded" src="<?php echo $this->config->item('url_upload_slider') . $row->slider_image_thumbs; ?>">
                    <?php endif; ?>
                <?php else: ?>
                    <!--OTHER FIELDS-->
                    <input class="span3" type="text" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>"  placeholder="<?php echo ucwords(preg_replace('/[_]/', ' ', $field->name)); ?>" value="<?php echo (set_value($field->name)) ? set_value($field->name) : (isset($row->{$field->name}) ? $row->{$field->name} : ''); ?>">
                <?php endif; ?>

                <?php echo form_error($field->name); ?>
            </div>    
        </div>


    <?php endforeach; ?>

    <?php if (preg_match('/^edit$/i', $action)): ?>
        <div class="control-group">
            <label class="control-label" for="delete_image_file">Hapus File Gambar</label>
            <div class="controls">
                <?php echo form_dropdown('delete_image_file', array('YES' => 'YES', 'NO' => 'NO'), 'NO', 'class="span3"'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php echo form_close(); ?>
</div>


<?php
//echo '<pre>'; print_r($enum);
//echo '<pre>'; print_r($row); 
//echo '<pre>'; print_r($fields);
?>