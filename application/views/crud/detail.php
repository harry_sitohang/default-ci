<div class="container">
    <div class="pull-right" style="margin: 20px 0" id="top"><a class="btn btn-info btn-back" href="<?php echo $class_url; ?>"><i class=" icon-arrow-left icon-white"></i>&nbsp;Back</a></div>
    <table class="table table-striped table-condensed">
        <?php foreach ($row as $key => $val): ?>
            <?php if (!in_array($key, $unset_fields)): ?>
                <tr>
                    <th><?php echo ucwords(preg_replace('/[_]/', ' ', $key)); ?></th>
                    <td>
                        <?php
                        if (isset($fields_data[$key]) && preg_match('/^(date|datetime)$/i', $fields_data[$key]->type)) {
                            echo format_date($val);
                        } else if (isset($fields_data[$key]) && preg_match('/^(file)$/i', $fields_data[$key]->type)) {
                            echo '<img class="img-rounded" src=' . $fields_data[$key]->url_upload . '/' . $row->{$fields_data[$key]->return_image} . '>';
                        } else {
                            echo $val;
                        }
                        ?>

                    </td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </table>
</div>