<style type="text/css">
    .tooltip {
        z-index: 9999 !important;
    }   

    .error-msg{
        color:#b94a48;
    }
</style>
<?php echo $my_style; ?>

<!-- BEGIN GLOBAL MANDATORY STYLES -->        
<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>css/style-metro.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
<!--<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>-->
<!-- END PAGE LEVEL PLUGIN STYLES -->

<!-- BEGIN PAGE LEVEL STYLES --> 
<!--<link href="<?php echo "{$assets_url}metronic/"; ?>css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>-->
<!-- END PAGE LEVEL STYLES -->


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->   
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>     
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>

<!--[if lt IE 9]>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/excanvas.min.js"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/respond.min.js"></script>  
<![endif]-->   

<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  -->
<!--<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/flot/jquery.flot.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
<script src="<?php echo "{$assets_url}metronic/"; ?>plugins/jquery.sparkline.min.js" type="text/javascript"></script>  -->
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->

<link href="<?php echo "{$assets_url}metronic/"; ?>plugins/select2/select2_metro.css" rel="stylesheet" type="text/css" media="screen"/>

<script src="<?php echo "{$assets_url}metronic/"; ?>scripts/app.js" type="text/javascript"></script>
<!--<script src="<?php echo "{$assets_url}metronic/"; ?>scripts/index.js" type="text/javascript"></script>-->
<!--<script src="<?php echo "{$assets_url}metronic/"; ?>scripts/tasks.js" type="text/javascript"></script>        -->
<script type="text/javascript" src="<?php echo "{$assets_url}metronic/"; ?>plugins/select2/select2.min.js"></script>


<!-- END PAGE LEVEL SCRIPTS -->  

<?php echo $my_js; ?>

<script type="text/javascript">
    jQuery(document).ready(function() {    
        App.init(); // initlayout and core plugins
        //        Index.init();
        //        Index.initJQVMAP(); // init index page's custom scripts
        //        Index.initCalendar(); // init index page's custom scripts
        //        Index.initCharts(); // init index page's custom scripts
        //        Index.initChat();
        //        Index.initMiniCharts();
        //        Index.initDashboardDaterange();
        //        Index.initIntro();
        //        Tasks.initDashboardWidget();
        $(".select2_sample3").select2({
            tags: ["red", "green", "blue", "yellow", "pink"]
        });
    });
</script>
<!-- END JAVASCRIPTS -->

