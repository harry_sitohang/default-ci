<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 2.3.2
Version: 1.4
Author: KeenThemes
Website: http://www.keenthemes.com/preview/?theme=metronic
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>Metronic | Admin Dashboard Template</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <script type="text/javascript">
            var ASSETS_URL = <?php echo json_encode($assets_url); ?>;
            var ASSETS_JS = <?php echo json_encode($assets_js); ?>;
            var ASSETS_CSS = <?php echo json_encode($assets_css); ?>;
            var BASE_URL = <?php echo json_encode($base_url); ?>;
            var CLASS_URL = <?php echo json_encode($class_url); ?>;
            var SCROLL_MARGIN_TOP = 80;
        </script>

        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->
    
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <?php $this->mytemplate->load_use_path("header"); ?>
        
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            
            <?php $this->mytemplate->load_use_path("sidebar"); ?>
            
            <!-- BEGIN PAGE -->
            <div class="page-content">
                <?php $this->mytemplate->load_use_path("portlet_config_modal_form"); ?>

                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <?php $this->mytemplate->load_use_path("page_header"); ?>
                    
                    <div id="dashboard">
                        <?php echo $main_view; ?>

                         <?php //$this->mytemplate->load_use_path("dashboard_stat"); ?>
                        
                        <div class="clearfix"></div>
                        
                        <?php //$this->mytemplate->load_use_path("portlet"); ?>
                        
                        <div class="clearfix"></div>
                        
                        <div class="row-fluid">
                            <?php //$this->mytemplate->load_use_path("recent_activities"); ?>
                            <?php //$this->mytemplate->load_use_path("task"); ?>
                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <?php //$this->mytemplate->load_use_path("stats"); ?>
                        
                        <div class="clearfix"></div>
                        
                        <div class="row-fluid">
                            <?php //$this->mytemplate->load_use_path("portlet_regional_stats"); ?>
                            <?php //$this->mytemplate->load_use_path("feeds"); ?>    
                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <div class="row-fluid">
                             <?php //$this->mytemplate->load_use_path("calendar"); ?>
                             <?php //$this->mytemplate->load_use_path("chats"); ?>
                        </div>
                    </div>
                    
                </div>
                <!-- END PAGE CONTAINER-->    
            </div>
            <!-- END PAGE -->
        </div>
        <!-- END CONTAINER -->
                
        <?php $this->mytemplate->load_use_path("footer"); ?>
        
        <?php $this->mytemplate->load_use_path("assets"); ?>
        
    </body>
    <!-- END BODY -->
</html>