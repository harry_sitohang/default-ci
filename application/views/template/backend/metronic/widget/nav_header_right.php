<!-- BEGIN TOP NAVIGATION MENU -->              
<ul class="nav pull-right">
    <!-- BEGIN NOTIFICATION DROPDOWN -->   
    <li class="dropdown" id="header_notification_bar">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <i class="icon-warning-sign"></i>
            <span class="badge">6</span>
        </a>
        <?php $this->mytemplate->load_use_path("widget/notification"); ?>
    </li>
    <!-- END NOTIFICATION DROPDOWN -->
    
    <!-- BEGIN INBOX DROPDOWN -->
    <li class="dropdown" id="header_inbox_bar">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <i class="icon-envelope"></i>
            <span class="badge">5</span>
        </a>
        <?php $this->mytemplate->load_use_path("widget/inbox"); ?>
    </li>
    <!-- END INBOX DROPDOWN -->
    
    <!-- BEGIN TODO DROPDOWN -->
    <li class="dropdown" id="header_task_bar">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <i class="icon-tasks"></i>
            <span class="badge">5</span>
        </a>
        <?php $this->mytemplate->load_use_path("widget/todo"); ?>
    </li>
    <!-- END TODO DROPDOWN -->            
    
    <!-- BEGIN USER LOGIN DROPDOWN -->
    <li class="dropdown user">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <img alt="" src="<?php echo "{$assets_url}metronic/"; ?>img/avatar1_small.jpg" />
            <span class="username">Bob Nilson</span>
            <i class="icon-angle-down"></i>
        </a>
        <?php $this->mytemplate->load_use_path("widget/login_menu"); ?>
    </li>
    <!-- END USER LOGIN DROPDOWN -->
    
</ul>
<!-- END TOP NAVIGATION MENU --> 