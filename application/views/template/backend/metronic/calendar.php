<div class="span6">
    <!-- BEGIN PORTLET-->
    <div class="portlet box blue calendar">
        <div class="portlet-title">
            <div class="caption"><i class="icon-calendar"></i>Calendar</div>
        </div>
        <div class="portlet-body light-grey">
            <div id="calendar"></div>
        </div>
    </div>
    <!-- END PORTLET-->
</div>