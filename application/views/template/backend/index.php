<?php extract($data); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Backend</title>
        <link rel="shortcut icon" type="image/x-icon" href="">

        <script type="text/javascript">
            var ASSETS_URL = <?php echo json_encode($assets_url); ?>;
            var ASSETS_JS = <?php echo json_encode($assets_js); ?>;
            var ASSETS_CSS = <?php echo json_encode($assets_css); ?>;
            var BASE_URL = <?php echo json_encode($base_url); ?>;
            var CLASS_URL = <?php echo json_encode($class_url); ?>;
        </script>

        <?php echo $my_style; ?>
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(sprintf('assets/bootstrap/css/bootstrap%s.css', $this->config->item('minified_version') === TRUE ? '.min' : '')); ?>" />
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }   
        </style>
        <!--Bootsrap CSS-->
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo sprintf('%ssite%s.css', $assets_css, $this->config->item('minified_version') === TRUE ? '.min' : ''); ?>" />
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(sprintf('assets/bootstrap/css/bootstrap-responsive%s.css', $this->config->item('minified_version') === TRUE ? '.min' : '')); ?>" />

        <!--Ace Admin CSS-->
        <link rel="stylesheet" href="<?php echo "{$assets_url}ace/css/font-awesome.min.css"; ?>" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="<?php echo "{$assets_url}ace/css/font-awesome-ie7.min.css"; ?>" />
        <![endif]-->
        <?php echo $my_js; ?>
        <link rel="stylesheet" href="<?php echo "{$assets_url}ace/css/ace.min.css"; ?>" />
        <link rel="stylesheet" href="<?php echo "{$assets_url}ace/css/ace-responsive.min.css"; ?>" />
        <link rel="stylesheet" href="<?php echo "{$assets_url}ace/css/ace-skins.min.css"; ?>" />
        <!--[if lte IE 8]>
          <link rel="stylesheet" href="<?php echo "{$assets_url}ace/css/ace-ie.min.css"; ?>assets/css/ace-ie.min.css" />
        <![endif]-->

    </head>
    <body>

        <?php echo $header_view; ?>
        <?php echo $menu_view; ?>

        <div class="container">
            <?php echo $main_view; ?>
        </div>

        <?php echo $footer_view; ?>

        <a href="#" class="scrollup">Scroll</a>
    </body>
</html>

<?php
//echo '<pre>';
//print_r($data);
//echo '</pre>';
?>