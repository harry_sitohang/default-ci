<div class="btn-toolbar">
    <div class="btn-group">
        <a class="btn btn-mini" rel="tooltip" title="Edit" href="<?php echo "{$class_url}edit/{$row->$PK}"; ?>" action="dt-edit"><i class="icon-pencil"></i></a>
        <a class="btn btn-mini" rel="tooltip" title="Delete" href="<?php echo "{$class_url}delete/{$row->$PK}"; ?>" action="dt-del"><i class="icon-trash"></i></a>
        <a class="btn btn-mini" rel="tooltip" title="Detail" href="<?php echo "{$class_url}detail/{$row->$PK}"; ?>" action="dt-view"><i class="icon-zoom-in"></i></a>
    </div>
</div>
