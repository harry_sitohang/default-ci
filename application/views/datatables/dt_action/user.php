<div class="btn-toolbar">
    <div class="btn-group">
        <a rel="tooltip" title="Edit" class="btn btn-mini" href="<?php echo "{$class_url}edit/{$row->$PK}"; ?>" action="dt-edit"><i class="icon-pencil"></i></a>
        <?php if (!preg_match('/^(1|2)/i', $row->type)): ?>
            <a rel="tooltip" title="Delete" class="btn btn-mini" href="<?php echo "{$class_url}delete/{$row->$PK}"; ?>" action="dt-del"><i class="icon-trash"></i></a>
        <?php endif; ?>
        <a rel="tooltip" title="Reset Password" class="btn btn-mini" href="<?php echo "{$class_url}reset_password/{$row->$PK}"; ?>" action="dt-reset-password"><i class="icon-repeat"></i></a>
        <a rel="tooltip" title="Detail" class="btn btn-mini" href="<?php echo "{$class_url}detail/{$row->$PK}"; ?>" action="dt-view"><i class="icon-zoom-in"></i></a>
    </div>
</div>