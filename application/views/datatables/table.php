<?php if (preg_match('/^modal$/i', $request_type)): ?>
    <div class="modal hide fade" id="datatable-modal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="header-title"></h3>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
            <a href="#" class="btn close-datatable-modal">Close</a>
            <a href="#" class="btn btn-primary" id="btn-datatable-action"></a>
        </div>
    </div>
<?php endif; ?>

<?php if (preg_match('/^tab/i', $request_type)): ?>
    <div class="tabbable tabbable-custom">
        <ul class="nav nav-tabs" id="datatable-tab" style="margin-top: 25px;">
            <li class="active"><a href="#grid-tab">Data Grid</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="grid-tab">
            <?php endif; ?>

            <div class="clearfix" style="margin: 10px 0;">
                <div class="pull-right">
                    <div class="btn-group">
                        <a class="btn" href="#">Export to : </a>
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo "{$class_url}dt_server_processing/xls"; ?>" action="dt-export-xls">XLS</a></li>
                        </ul>
                    </div>
                    <a class="refresh btn" href="#"><i class="icon-refresh"></i>&nbsp;refresh</a>
                    <a class="btn" action="dt-add" href="<?php echo "{$class_url}add"; ?>"><i class="icon-plus"></i>&nbsp;add</a>
                </div>
            </div>

            <table class="table table-striped table-bordered table-condensed" id="datatable">
                <?php echo $columns_header; ?>
                <tbody>  
                    <tr>
                        <td colspan="<?php echo count($fields) + ($number_counter === TRUE ? 1 : 0) + ($column_action === TRUE ? 1 : 0) - count($unset_fields); ?>">Loading data from server</td>
                    </tr>
                </tbody>
            </table>
                
            <?php if (preg_match('/^tab/i', $request_type)): ?>
            </div>
        </div>
    </div>
<?php endif; ?>




<!--ACTION EDIT/ADD CSS-JS-->
<!--<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url(); ?>assets/jquery-ui/time-picker/jquery-ui-timepicker-addon.css" />
<script src="<?php echo base_url(); ?>assets/jquery-ui/time-picker/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/jquery-ui/time-picker/jquery-ui-sliderAccess.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/tinymce/tinymce.init.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/tinymce/tinymce_upload.js" type="text/javascript"></script>
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url(); ?>assets/tinymce/tinymce_upload.css" />
<script src="<?php echo base_url(); ?>assets/js/oTable.js" type="text/javascript"></script>-->
<!--<script src="<?php echo base_url(); ?>assets/js/crud.js" type="text/javascript"></script>-->


<!--Tiny mce form upload-->
<?php //$this->mytemplate->load("tinymce/form_upload"); ?>
<?php $this->mytemplate->load("tinymce/media_dialog"); ?>
