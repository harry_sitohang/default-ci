<thead>
    <tr>
        <?php if ($number_counter === TRUE): ?>
            <th>No</th>
        <?php endif; ?>

        <?php foreach ($fields as $field): ?>
            <th><?php echo ucwords(preg_replace('/[_]/', ' ', $field)); ?></th>
        <?php endforeach; ?>

        <?php if ($column_action === TRUE): ?>
            <th>&nbsp;</th>
        <?php endif; ?>
    </tr>
</thead>