<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once  APPPATH . 'core/controllers/master'.EXT;

class Home extends Master {

    public function __construct() {
        parent::__construct(array(
            'controller_type' => 'frontend'
        ));
    }
    
    public function index() {
        $this->mytemplate->set_main_view("{$this->views_path}home");
        $this->mytemplate->generate();
    }

}

/* End of file home.php */
/* Location: ./application/controllers/frontend/home.php */