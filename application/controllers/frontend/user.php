<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once  APPPATH . 'core/controllers/master'.EXT;

class User extends Master {

    public $controller_type = 'frontend';

    public function __construct() {
        parent::__construct(array(
            'controller_type' => $this->controller_type
        ));
    }
    


}

/* End of file user.php */
/* Location: ./application/controllers/frontend/user.php */