<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/controllers/datatable' . EXT;

class Menu_management extends Datatable {

    public $controller_type = 'backend';
    public $my_model = 'Menu_management_model';

    public function __construct() {
        parent::__construct(array(
            'controller_type' => $this->controller_type, // MASTER PARAMETER
            'request_type' => 'tab', //CRUD PARAMETER
            'my_model' => $this->my_model, //DATATABLE PARAMETER
            'class_name' => get_class($this), //DATATABLE PARAMETER
            //'number_counter' => FALSE,
            //'column_action' => FALSE,
            'fields' => array()//array('type', 'email', 'password', 'last_ip', 'last_login', 'activated', 'banned'), //DATATABLE PARAMETER
                //'hidden_fields' => array('content'),
                //'show_pk' => TRUE
        ));
    }

    /**
     * @desc customize crud editor view
     * @override 
     */
    public function set_editor_view_ajax($action, $fields, $enum, $relation, $id = '') {
        //print_r($this->file_fields); die;
        $row = $this->{$this->my_model}->get_by_id($id, '*')->row();
        $data = array(
            'modal_body' => $this->mytemplate->load('crud/edit/menu_management_body', array(
                'fields' => $fields,
                'pk' => $this->{$this->my_model}->PK,
                'table' => ucwords(preg_replace('/[_]/', ' ', $this->{$this->my_model}->table)),
                'row' => $row,
                'enum' => $enum,
                'relation' => $relation,
                'action' => $action,
                'multipart' => (!empty($this->file_fields)) ? TRUE : FALSE,
                'pattern_unset_fields' => '/^(' . implode('|', $this->unset_fields) . ')$/i',
                'request_type' => $this->request_type,
                'menu_dropdown' => $this->generate_menu_selected_parent_id(isset($row->parent_id) ? $row->parent_id : 0, $id)
                    ), TRUE),
            'modal_header_title' => ucwords(preg_replace('/[_]/', ' ', $this->{$this->my_model}->table)),
            'action' => $action,
            'request_type' => $this->request_type
        );

        if (preg_match('/^tab$/i', $this->request_type)) {
            if (preg_match('/^edit$/i', $action)) {
                $data['tab_li_title'] = $this->set_tab_li_title($row);
                $data['id'] = $id;
                $data['modal_action'] = $this->mytemplate->load('crud/action', array('action' => 'edit', 'id' => $id), TRUE);
            } else {
                $data['modal_action'] = $this->mytemplate->load('crud/action', array('action' => 'add'), TRUE);
            }
        }

        echo json_encode($data);
    }

    public function fields_data_custom($field_data) {
        $field_data = parent::fields_data_custom($field_data);
        $field_data['menu_url_segment']->is_unique = "YES";
        return $field_data;
    }

    public function check_is_unique_field($val, $field_name) {
        return parent::check_is_unique_field(convert_to_string_url($val), $field_name);
    }

    public function customize_data_before_crud($data) {
        $data = parent::customize_data_before_crud($data);
        $data["menu_url_segment"] = convert_to_string_url($data["menu_url_segment"]);
        return $data;
    }

    public function set_tab_li_title($row) {
        return $row->menu_url_segment;
    }

    public function generate_menu_selected_parent_id($selected_parent_menu_id, $selected_menu_id, $parent_id = 0, $parent_segment = '', $opacity = 10, $sign_segment = "") {
        $option = "";

        if ($parent_id == 0) {
            $option = "<select name=\"parent_id\" id=\"parent_id\" class=\"span3\">";
            $option .= sprintf("<option value=\"%d\">New Parent</option>", 0);
        }

        $this->db->select("*");
        $this->db->from("menu");
        $this->db->where(array("parent_id" => $parent_id, "menu_id <>" => $selected_menu_id));
        $result = $this->db->get()->result();

        foreach ($result as $row) {
            $menu_segment = empty($parent_segment) ? $row->menu_url_segment : sprintf("%s/%s", $parent_segment, $row->menu_url_segment);
            $option .= sprintf("<option style=\"%s\" value=\"%d\" %s segment=\"%s\">%s%s</option>", 'color:#333; background: rgba(162, 168, 168, ' . ($opacity / 10) . ');', $row->menu_id, ($row->menu_id == $selected_parent_menu_id) ? 'selected="true"' : '', $menu_segment, $sign_segment, $row->menu_name);

            $this->db->select("COUNT(1) AS `count`");
            $this->db->from("menu");
            $this->db->where(array("parent_id" => $row->menu_id));
            $count = $this->db->get()->row()->count;
            if ($count != 0) {
                $option .= $this->generate_menu_selected_parent_id($selected_parent_menu_id, $selected_menu_id, $row->menu_id, $menu_segment, ($opacity - 2 < 0) ? 0 : $opacity - 2, "----- " . $sign_segment);
            }
        }

        if ($parent_id == 0) {
            $option .= "</select>";
        }
        return $option;
    }

}

/* End of file ci_sessions.php */
/* Location: ./application/controllers/backend/ci_sessions.php */