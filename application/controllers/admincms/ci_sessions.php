<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/controllers/datatable' . EXT;

class Ci_sessions extends Datatable {

    public $controller_type = 'backend';
    public $my_model = 'Ci_sessions_model';

    public function __construct() {
        parent::__construct(array(
            'controller_type' => $this->controller_type, // MASTER PARAMETER
            'request_type' => 'tab', //CRUD PARAMETER
            'my_model' => $this->my_model, //DATATABLE PARAMETER
            'class_name' => get_class($this), //DATATABLE PARAMETER
            //'number_counter' => FALSE,
            //'column_action' => FALSE,
            'fields' => array()//array('type', 'email', 'password', 'last_ip', 'last_login', 'activated', 'banned'), //DATATABLE PARAMETER
                //'hidden_fields' => array('content'),
                //'show_pk' => TRUE
        ));
    }

}

/* End of file ci_sessions.php */
/* Location: ./application/controllers/backend/ci_sessions.php */