<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/controllers/datatable' . EXT;

class Meta extends Datatable {

    public $controller_type = 'backend';
    public $my_model = 'Meta_model';

    public function __construct() {
        parent::__construct(array(
            'controller_type' => $this->controller_type, // MASTER PARAMETER
            'request_type' => 'tab', //CRUD PARAMETER
            'my_model' => $this->my_model, //DATATABLE PARAMETER
            'class_name' => get_class($this), //DATATABLE PARAMETER
            //'number_counter' => FALSE,
            //'column_action' => FALSE,
            'fields' => array()//array('type', 'email', 'password', 'last_ip', 'last_login', 'activated', 'banned'), //DATATABLE PARAMETER
                //'hidden_fields' => array('content'),
                //'show_pk' => TRUE
        ));
    }

    /**
     * @override CRUD
     */
    public function set_tab_li_title($row) {
        return $row->uri_segment;
    }

    public function fields_data_custom($field_data) {
        //$field_data['meta_title']->default = "dicky homo";
        //$field_data['meta_title']->required = "YES";
        $field_data['meta_title']->min_length = 30;
        $field_data['meta_title']->plugin = "char_count";
        //$field_data['meta_title']->char_count = "min_length=30|max_length={$field_data['meta_title']->max_length}";
        
        $field_data['meta_desc']->class = "span6";
        $field_data['meta_desc']->min_length = 100;
        $field_data['meta_desc']->plugin = "char_count";
        
        $field_data['meta_keywords']->required = "NO";
        
        return $field_data;
    }

    /**
     * @desc VALIDATION RULES
     * @return array
     */
    public function validation_rules($action) {
        $config = parent::validation_rules($action);
        //print_r($config); die;
        $config["uri_segment"]['rules'] .= '|callback_is_valid_uri_segment';
        
        
        return $config;
    }

    /**
     * @desc callback check is valid uri_segment
     * @return boolean
     */
    public function is_valid_uri_segment($uri_segment) {
        $this->db->select("COUNT(1) AS `count`");
        $this->db->from("meta");
        $this->db->where(array("uri_segment" => $uri_segment));
        $count = $this->db->get()->row()->count;
        if ($count == 0) {
            return TRUE;
        }

        if (preg_match("/^add$/i", $this->input->post("action"))) {
            $this->form_validation->set_message('is_valid_uri_segment', 'Uri segment, has been used, please try another uri segment');
            return FALSE;
        } else {
            $this->db->select("uri_segment");
            $this->db->from("meta");
            $this->db->where(array("meta_id" => $this->input->post("meta_id")));
            $row = $this->db->get()->row();
            if ($row->uri_segment == $uri_segment) {
                return TRUE;
            } else {
                $this->form_validation->set_message('is_valid_uri_segment', 'Uri segment, has been used, please try another uri segment');
                return FALSE;
            }
        }
    }

}

/* End of file meta.php */
/* Location: ./application/controllers/backend/page.php */