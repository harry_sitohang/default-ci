<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/controllers/datatable' . EXT;

class Slider extends Datatable {

    public $controller_type = 'backend';
    public $my_model = 'Slider_model';

    public function __construct() {
        parent::__construct(array(
            'controller_type' => $this->controller_type, // MASTER PARAMETER
            'request_type' => 'tab', //CRUD PARAMETER
            'my_model' => $this->my_model, //DATATABLE PARAMETER
            'class_name' => get_class($this), //DATATABLE PARAMETER
            //'number_counter' => FALSE,
            //'column_action' => FALSE,
            'fields' => array('slider_caption', 'slider_image_thumbs', 'link_url', 'active')
                //'hidden_fields' => array('content'),
                //'show_pk' => TRUE
        ));
    }

    /**
     * @override DATATABLE
     */
    public function set_column_header($fields) {
        //  print_r($fields); die;
        return $this->mytemplate->load('datatables/heads', array('fields' => array('slider_id', 'slider_caption', 'Deskripsi Slider', 'link_url', 'active'), 'number_counter' => $this->number_counter, 'column_action' => $this->column_action), TRUE);
    }

    /**
     * @override DATATABLE
     */
    function set_row($data, $aRow, $number) {
        $row = array();
        $this->load->helper('text');

        //IF NUMBER COUNTER SET TO TRUE 
        if ($this->number_counter === TRUE) {
            $row[] = $number;
        }

        foreach ($data['aColumns'] as $aColumn) {
            if (preg_match('/^slider_image_thumbs$/i', $aColumn)) {
                if (preg_match('/^FILE$/i', $aRow->image_used)) {
                    $row[] = '<img class="img-rounded" src=' . $this->config->item('url_upload_slider') . $aRow->$aColumn . '>';
                } else {
                    $row[] = '<img class="img-rounded" src=' . $aRow->image_url_thumbs . '>';
                }
            } else if (preg_match('/^link_url$/i', $aColumn)) {
                if (preg_match('/^YES$/i', $aRow->is_a_link)) {
                    $row[] = $aRow->$aColumn;
                } else {
                    $row[] = '-';
                }
            } else if (preg_match('/^slider_caption$/i', $aColumn)) {
                $row[] = word_limiter(strip_tags($aRow->$aColumn), 15);
            } else {
                $row[] = $aRow->$aColumn;
            }
        }

        //IF COLUMN ACTION === TRUE
        if ($this->column_action === TRUE) {
            $row[] = $this->set_action_row($aRow);
        }

        return $row;
    }

    /**
     * @override CRUD
     */
    public function set_tab_li_title($row) {
        return $row->slider_caption;
    }

    public function set_editor_view_ajax($action, $fields, $enum, $relation, $id = '') {
        print_r($this->file_fields); die;
        $row = $this->{$this->my_model}->get_by_id($id, '*')->row();
        $data = array(
            'modal_body' => $this->mytemplate->load('crud/edit/slider_body', array(
                'fields' => $fields,
                'pk' => $this->{$this->my_model}->PK,
                'table' => ucwords(preg_replace('/[_]/', ' ', $this->{$this->my_model}->table)),
                'row' => $row,
                'enum' => $enum,
                'relation' => $relation,
                'action' => $action,
                'multipart' => (!empty($this->file_fields)) ? TRUE : FALSE,
                'pattern_unset_fields' => '/^(' . implode('|', $this->unset_fields) . ')$/i'
                    ), TRUE),
            'modal_header_title' => ucwords(preg_replace('/[_]/', ' ', $this->{$this->my_model}->table)),
            'action' => $action,
            'request_type' => $this->request_type
        );

        if (preg_match('/^tab$/i', $this->request_type)) {
            if (preg_match('/^edit$/i', $action)) {
                $data['tab_li_title'] = $this->set_tab_li_title($row);
                $data['id'] = $id;
                $data['modal_action'] = $this->mytemplate->load('crud/action', array('action' => 'edit', 'id' => $id), TRUE);
            } else {
                $data['modal_action'] = $this->mytemplate->load('crud/action', array('action' => 'add'), TRUE);
            }
        }

        echo json_encode($data);
    }

    /**
     * @override CRUD
     */
    public function validation_rules($action) {
        $config = parent::validation_rules($action);
        if ((preg_match('/^edit$/i', $action) && preg_match('/^FILE$/i', $this->input->post('image_used')) && $this->input->post('change_slider_image')) || (preg_match('/^add$/i', $action) && preg_match('/^FILE$/i', $this->input->post('image_used')))) {
            $new_config = array(
                'slider_image' => array(
                    'field' => 'slider_image',
                    'label' => 'Slider Image',
                    'rules' => 'callback_check_slider_image'
                )
            );
        } else {
            $new_config = array(
                'slider_image' => array(
                    'field' => 'slider_image',
                    'label' => 'Slider Image',
                    'rules' => 'xss_clean'
                ),
                'slider_image_thumbs' => array(
                    'field' => 'slider_image_thumbs',
                    'label' => 'Slider Image Thumbs',
                    'rules' => 'xss_clean'
                )
            );
        }


        if (preg_match('/^FILE$/i', $this->input->post('image_used'))) {
            $new_config['image_url'] = array(
                'field' => 'image_url',
                'label' => 'Image Url',
                'rules' => 'xss_clean'
            );
            $new_config['image_url_thumbs'] = array(
                'field' => 'image_url_thumbs',
                'label' => 'Image Url Thumbs',
                'rules' => 'xss_clean'
            );
        }

        if (preg_match('/^NO$/i', $this->input->post('is_a_link'))) {
            $new_config['link_url'] = array(
                'field' => 'link_url',
                'label' => 'Link Url',
                'rules' => 'xss_clean'
            );
        }
        //echo '<pre>'; print_r(array_merge($config, $new_config)); die;
        return array_merge($config, $new_config);
    }

    /**
     * @override CRUD
     */
    public function file_validation($action) {
        return array(); //DISABLE automatic array validation
    }

    /**
     * @override CRUD
     */
    public function customize_field($fields, $action) {
        $pattern = '/^slider_image_thumbs$/i';
        foreach ($fields as $field_key => $field) {
            if (preg_match($pattern, $field->name)) {
                unset($fields[$field_key]);
            }
        }
        //print_r($fields); die;
        return $fields;
    }

    /**
     * @override CRUD
     */
    public function customize_data_before_crud($data) {
        //echo '<pre>';        print_r($data); die;
        if ((preg_match('/^edit$/i', $this->input->post('action')) && preg_match('/^FILE$/i', $this->input->post('image_used')) && $this->input->post('change_slider_image')) || (preg_match('/^add$/i', $this->input->post('action')) && preg_match('/^FILE$/i', $this->input->post('image_used')))) {
            $data['slider_image_thumbs'] = $this->input->post('slider_image_thumbs');
            unset($data['image_url']);
            unset($data['image_url_thumbs']);
            if (preg_match('/^edit$/i', $this->input->post('action'))) {
                $this->delete_old_files($this->input->post('slider_id'));
            }
        } else {
            unset($data['slider_image']);
            unset($data['slider_image_thumbs']);
            if (preg_match('/^edit$/i', $this->input->post('action')) && preg_match('/^YES$/i', $this->input->post('delete_image_file'))) {
                $this->delete_old_files($this->input->post('slider_id'));
            }
        }

        if (preg_match('/^NO$/i', $this->input->post('is_a_link'))) {
            unset($data['link_url']);
        }

        return $data;
    }

    public function delete_old_files($id) {
        //DELETE OLD IMAGES
        $row = $this->{$this->my_model}->get_by_id($id, '*')->row();
        $path_upload_slider_image = $this->config->item('path_upload_slider_image');

        if (file_exists($path_upload_slider_image['upload_path'] . $row->slider_image) && !is_dir($path_upload_slider_image['upload_path'] . $row->slider_image)) {
            unlink($path_upload_slider_image['upload_path'] . $row->slider_image);
        }

        if (file_exists($path_upload_slider_image['upload_path'] . $row->slider_image_thumbs) && !is_dir($path_upload_slider_image['upload_path'] . $row->slider_image)) {
            unlink($path_upload_slider_image['upload_path'] . $row->slider_image_thumbs);
        }
    }

    /**
     * @override CRUD
     */
    public function delete($id) {
        //DELETE IMAGES
        $this->delete_old_files($id);
        parent::delete($id);
    }

    /**
     * @desc callback check slider uploaded image
     */
    public function check_slider_image() {
        //upload image
        if ($this->uploadImage('slider_image', $this->config->item('path_upload_slider_image'))) {
            $upload_data = $this->upload->data();
            $resize_config = $this->config->item('resize_slider_image');
            //resize ori image
            if ($this->resizeOriImage($upload_data['full_path'], $resize_config['big']['w'], $resize_config['big']['h'])) {
                //resize image, creating thumbs
                if ($this->resizeImage($upload_data['full_path'], $resize_config['thumbs']['w'], $resize_config['thumbs']['h'])) {
                    //SET POST variable
                    $_POST['slider_image'] = $upload_data['file_name'];
                    $_POST['slider_image_thumbs'] = $upload_data['raw_name'] . '_thumb' . $upload_data['file_ext'];
                    return TRUE;
                } else {
                    $this->form_validation->set_message('check_slider_image', $this->image_lib->display_errors());
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('check_slider_image', $this->image_lib->display_errors());
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('check_slider_image', $this->upload->display_errors());
            return FALSE;
        }
    }

}

/* End of file slider.php */
/* Location: ./application/controllers/backend/slider.php */