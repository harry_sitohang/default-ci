<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/controllers/datatable' . EXT;

class User extends Datatable {

    public $controller_type = 'backend';
    public $my_model = 'User_model';

    public function __construct() {
        parent::__construct(array(
            'controller_type' => $this->controller_type, // MASTER PARAMETER
            'request_type' => 'tab', //CRUD PARAMETER
            'my_model' => $this->my_model, //DATATABLE PARAMETER
            'class_name' => get_class($this), //DATATABLE PARAMETER
            //'number_counter' => FALSE,
            //'column_action' => FALSE,
            'fields' => array()//array('type', 'email', 'password', 'last_ip', 'last_login', 'activated', 'banned'), //DATATABLE PARAMETER
                //'hidden_fields' => array('content'),
                //'show_pk' => TRUE
        ));
    }
    
    
    public function fields_data_custom($field_data) {
        $field_data['type']->type = 'relation';
        $field_data['type']->relation = "user_type|user_type_id|user_type";
        $field_data['type']->style =  "";
        return $field_data;
    }
    
    /**
     * @desc customize crud editor view
     * @override 
     */
    public function set_editor_view_ajax($action, $fields, $enum, $relation, $id = '') {
        //print_r($this->file_fields); die;
        $row = $this->{$this->my_model}->get_by_id($id, '*')->row();
        $data = array(
            'modal_body' => $this->mytemplate->load('crud/edit/user_body', array(
                'fields' => $fields,
                'pk' => $this->{$this->my_model}->PK,
                'table' => ucwords(preg_replace('/[_]/', ' ', $this->{$this->my_model}->table)),
                'row' => $row,
                'enum' => $enum,
                'relation' => $relation,
                'action' => $action,
                'multipart' => (!empty($this->file_fields)) ? TRUE : FALSE,
                'pattern_unset_fields' => '/^(' . implode('|', $this->unset_fields) . ')$/i',
                'request_type' => $this->request_type
                    ), TRUE),
            'modal_header_title' => ucwords(preg_replace('/[_]/', ' ', $this->{$this->my_model}->table)),
            'action' => $action,
            'request_type' => $this->request_type
        );

        if (preg_match('/^tab$/i', $this->request_type)) {
            if (preg_match('/^edit$/i', $action)) {
                $data['tab_li_title'] = $this->set_tab_li_title($row);
                $data['id'] = $id;
                $data['modal_action'] = $this->mytemplate->load('crud/action', array('action' => 'edit', 'id' => $id), TRUE);
            } else {
                $data['modal_action'] = $this->mytemplate->load('crud/action', array('action' => 'add'), TRUE);
            }
        }

        echo json_encode($data);
    }

    public function set_tab_li_title($row) {
        return $row->email;
    }

    public function delete($id) {
        if (preg_match('/^(1|2)$/', $id)) {
            if ($this->input->is_ajax_request()) {
                echo json_encode(array('status' => 'error', 'msg' => 'Tidak dapat menghapus akun "Admin"'));
                die;
            } else {
                redirect("{$this->views_path}{$this->class_name}");
            }
        }

        parent::delete($id);
    }
    
    public function validation_rules($action) {
        $config = parent::validation_rules($action);

        $new_config = array(
            'ban_reason' => array(
                'field' => 'ban_reason',
                'label' => 'Ban Reason',
                'rules' => 'xss_clean'
            ),
            'new_password_key' => array(
                'field' => 'new_password_key',
                'label' => 'New Password Key',
                'rules' => 'xss_clean'
            ),
            'new_password_key' => array(
                'field' => 'new_password_key',
                'label' => 'New Password Key',
                'rules' => 'xss_clean'
            ),
            'new_password_requested' => array(
                'field' => 'new_password_requested',
                'label' => 'New Password Requested',
                'rules' => 'xss_clean'
            ),
            'last_login' => array(
                'field' => 'last_login',
                'label' => 'Last Login',
                'rules' => 'xss_clean|strip_tags'
            ),
            'new_email' => array(
                'field' => 'new_email',
                'label' => 'New Email',
                'rules' => 'xss_clean'
            ),
            'new_email_key' => array(
                'field' => 'new_email_key',
                'label' => 'New Email Key',
                'rules' => 'xss_clean'
            ),
            'last_ip' => array(
                'field' => 'last_ip',
                'label' => 'Last Login',
                'rules' => 'xss_clean'
            )
        );

        if (preg_match('/^edit$/i', $this->input->post('action'))) {
            $new_config['password'] = array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'xss_clean'
            );
            $new_config['email'] = array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'xss_clean|trim|required|valid_email|callback_check_email_edit'
            );
        } else {
            $new_config['password'] = array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'xss_clean|trim|required|max_length[100]'
            );
            $new_config['email'] = array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'xss_clean|trim|required|valid_email|callback_check_email_add'
            );
        }

        //print_r($config); die;
        return array_merge($config, $new_config);
    }
    
    /**
     * @desc callback
     */
    public function check_email_add() {
        $row = $this->User_model->get_where('*', array('email' => $this->input->post('email')))->row();
        if ($this->input->post('email') && empty($row)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_email_add', 'Email yang Anda input sudah digunakan user lainnya');
            return FALSE;
        }
    }

    public function check_email_edit() {
        $row = $this->db->query("SELECT * FROM {$this->User_model->table} WHERE email = ? AND user_id <> ? ", array($this->input->post('email'), $this->input->post('user_id')))->row();
        if ($this->input->post('email') && empty($row)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_email_edit', 'Email yang Anda input sudah digunakan user lainnya');
            return FALSE;
        }
    }

}

/* End of file user.php */
/* Location: ./application/controllers/admincms/user.php */