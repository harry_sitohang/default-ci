<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once  APPPATH . 'core/controllers/master'.EXT;

class Setting extends Master {

    public $controller_type = 'backend';
    public $my_model = 'Setting_model';
    public $class_name = '';

    public function __construct() {
        parent::__construct(array(
            'controller_type' => $this->controller_type, // MASTER PARAMETER,
        ));
        $this->class_name = strtolower(get_class($this));
        $this->load->model($this->my_model);
    }

    public function index() {

        $this->load->helper('form');
        $fields = $this->{$this->my_model}->get_fields_data();

        $rules = ($this->input->post('change_icon_image')) ? 'edit-setting' : 'edit-setting-wo-image';

        if ($this->input->post('action') && $this->input->post('action') == 'edit') {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<label class="error-msg label-inline">', '</label>');
            if ($this->form_validation->run($rules) == TRUE) {
                $data = array();
                foreach ($fields as $field) {
                    if ($this->input->post($field->name)) {
                        $data[$field->name] = $this->input->post($field->name);
                    }
                }
                
                if($this->input->post('change_icon_image')){
                    $this->delete_old_files();
                }

                if ($this->db->update($this->{$this->my_model}->table, $data)) {
                    $this->session->set_flashdata('status', 'success');
                    redirect("{$this->views_path}{$this->class_name}");
                }
            } else {
                unset($_POST['action']); //UNSET action POST VARIABLE BEFORE SENDING IT BACK TO VIEW, TO HANDLE LOOPING FOREVER
                $this->index(); //SENDING BACK TO VIEW
            }
        } else {

            //SET ENUM DATA
            $enum = array();
            foreach ($fields as $field) {
                if (preg_match('/^enum$/i', $field->type)) {
                    $enum[$field->name] = $this->{$this->my_model}->get_enum_values($field->name);
                }
            }

            $this->mytemplate->set_main_view("{$this->views_path}setting");
            $this->mytemplate->set_data(array('fields' => $fields));
            $this->mytemplate->set_data(array('enum' => $enum));
            $this->mytemplate->set_data(array('data' => $this->{$this->my_model}->get_where('*')->row()));
            $this->mytemplate->generate();
        }
    }

    /**
     * @desc callback function
     */
    public function check_shortcut_icon() {
        //upload image
        if ($this->uploadImage('shortcut_icon', $this->config->item('path_upload_icon_image'))) {
            $upload_data = $this->upload->data();
            $resize_config = $this->config->item('resize_icon_image');
            //resize ori image
            if ($this->resizeOriImage($upload_data['full_path'], $resize_config['w'], $resize_config['h'])) {
                //SET POST variable
                $_POST['shortcut_icon'] = $upload_data['file_name'];
                return TRUE;
            } else {
                $this->form_validation->set_message('check_shortcut_icon', $this->image_lib->display_errors());
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('check_shortcut_icon', $this->upload->display_errors());
            return FALSE;
        }
    }
    
    
     public function delete_old_files() {
        //DELETE OLD IMAGES
        $row = $this->{$this->my_model}->get_where('*')->row();
        $path_upload_icon_image = $this->config->item('path_upload_icon_image');

        if (file_exists($path_upload_icon_image['upload_path'] . $row->shortcut_icon) && !is_dir($path_upload_icon_image['upload_path'] . $row->shortcut_icon)) {
            unlink($path_upload_icon_image['upload_path'] . $row->shortcut_icon);
        }

    }

}

/* End of file page.php */
/* Location: ./application/controllers/backend/page.php */