<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'core/controllers/datatable' . EXT;

class Test extends Datatable {

    public $controller_type = 'backend';
    public $my_model = 'Test_model';

    public function __construct() {
        parent::__construct(array(
            'controller_type' => $this->controller_type, // MASTER PARAMETER
            'request_type' => 'tab', //CRUD PARAMETER
            'my_model' => $this->my_model, //DATATABLE PARAMETER
            'class_name' => get_class($this), //DATATABLE PARAMETER
            //'number_counter' => FALSE,
            //'column_action' => FALSE,
            'fields' => array()//array('type', 'email', 'password', 'last_ip', 'last_login', 'activated', 'banned'), //DATATABLE PARAMETER
                //'hidden_fields' => array('content'),
                //'show_pk' => TRUE
        ));
    }

    /**
     * @override CRUD
     */
    public function set_tab_li_title($row) {
        return $row->title;
    }

    /**
     * @override DATATABLE
     */
//    function set_row($data, $aRow, $number) {
//        $row = array();
//        $this->load->helper('text');
//
//        //IF NUMBER COUNTER SET TO TRUE 
//        if ($this->number_counter === TRUE) {
//            $row[] = $number;
//        }
//
//        foreach ($data['aColumns'] as $aColumn) {
//            if (preg_match('/^desc$/i', $aColumn)) {
//                $row[] = word_limiter(strip_tags($aRow->$aColumn), 25);
//            } else {
//                $row[] = $aRow->$aColumn;
//            }
//        }
//
//        //IF COLUMN ACTION === TRUE
//        if ($this->column_action === TRUE) {
//            $row[] = $this->set_action_row($aRow);
//        }
//
//        return $row;
//    }

    public function check_image_ajax() {
        if ($this->input->is_ajax_request()) {
            //echo '<pre>'; print_r($_FILES); die;
            //print_r($this->file_fields); die;
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<label class="error-msg">', '</label>');

            $_POST['trigger_check_image'] = TRUE;
            foreach ($this->file_fields as $field) {
                //$_POST["{$field}"] = $field; //set field name for callback parameter value
                $this->form_validation->set_rules($field, ucwords(preg_replace('/[_]/', ' ', $field)), 'callback_check_image[' . $field . ']');
            }

            if ($this->form_validation->run() === TRUE) {
                //unset($_POST['field_name_callback']);
                unset($_POST['trigger_check_image']);
                echo json_encode(array('status' => 'success', 'files' => $_POST));
            } else {
                //unset($_POST['field_name_callback']);
                unset($_POST['trigger_check_image']);
                echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
            }
        }
    }

    public function check_image($str, $field) {
        //upload image
        $return = TRUE;
        if ($this->uploadImage("{$field}", $this->fields_data[$field]->upload)) {
            $upload_data = $this->upload->data();
            $resize_config = $this->fields_data[$field]->resize;

            foreach ($resize_config as $file_field_name => $resize_data) {
                
                $resize_response = ($resize_data['resize_ori_img'] === TRUE) ? $this->resizeOriImage($upload_data['full_path'], $resize_data['w'], $resize_data['h']) : $this->resizeImage($upload_data['full_path'], $resize_data['w'], $resize_data['h'], $resize_data['sufiks_filename']);
                //if($resize_data['sufiks_filename'] == "_thumb3"){echo $upload_data['full_path'];}
                if ($resize_response) {
                    $_POST["{$file_field_name}"] = $upload_data['raw_name'] . $resize_data['sufiks_filename'] . $upload_data['file_ext'];
                } else {
                    $this->form_validation->set_message('check_image', $this->image_lib->display_errors());
                    $return = FALSE;
                }
            }
        } else {
            $this->form_validation->set_message('check_image', $this->upload->display_errors());
            $return = FALSE;
        }

        if ($return === FALSE && $this->input->is_ajax_request()) {
            $this->delete_old_files_ajax();
        }

        return $return;
    }

    public function add() {
        //echo '<pre>'; print_r($_POST); die;
        $this->load->library('form_validation');
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_error_delimiters('<label class="error-msg">', '</label>');
        } else {
            $this->form_validation->set_error_delimiters('<label class="error-msg label-inline">', '</label>');
        }

        //$fields = $this->get_fields_data();
        //$fields = $this->customize_field($fields, 'add');
        $fields = $this->customize_field($this->fields_data, 'add');

        if ($this->input->post('action') && $this->input->post('action') == 'add') {
            //echo '<pre>'; print_r($this->validation_rules()); die;
            $this->form_validation->set_rules($this->validation_rules($this->input->post('action')));
            if ($this->form_validation->run() == TRUE) {
                $data = array();
                foreach ($fields as $field) {
                    if ($this->input->post($field->name)) {
                        $data[$field->name] = $this->input->post($field->name);
                    }
                }
                $data = $this->customize_data_before_crud($data);
                //echo '<pre>'; print_r($_POST); die;
                if ($this->db->insert($this->{$this->my_model}->table, $data)) {
                    if ($this->input->is_ajax_request()) {
                        echo json_encode(array('status' => 'success', 'msg' => "successfully {$this->input->post('action')} your data"));
                    } else {
                        $this->session->set_flashdata('status', 'success');
                        redirect("{$this->views_path}{$this->class_name}/add");
                    }
                } else {
                    if ($this->input->is_ajax_request()) {
                        echo json_encode(array('status' => 'error', 'msg' => mysql_error()));
                    }
                }
            } else {
                if ($this->input->is_ajax_request()) {
                    $this->delete_old_files_ajax();
                    echo json_encode(array('status' => 'error', 'msg' => validation_errors()));
                } else {
                    unset($_POST['action']); //UNSET action POST VARIABLE BEFORE SENDING IT BACK TO VIEW, TO HANDLE LOOPING FOREVER
                    $this->add(); //SENDING BACK TO VIEW
                }
            }
        } else {
            //SET ENUM AND RELATION DATA
            $enum = array();
            $relation = array();
            foreach ($fields as $field) {
                if (preg_match('/^enum$/i', $field->type)) {
                    $enum[$field->name] = $this->{$this->my_model}->get_enum_values($field->name);
                } else if (isset($field->relation)) {
                    $relation = explode('|', $field->relation);
                    $this->db->select(array($relation[1], $relation[2]));
                    $this->db->from($relation[0]);
                    $relation[$field->name] = create_form_dropdown_options($this->db->get()->result_array(), $relation[1], $relation[2]);
                }
            }

            if ($this->input->is_ajax_request()) {
                $this->set_editor_view_ajax('add', $fields, $enum, $relation);
            } else {
                $this->set_editor_view('add', $fields, $enum, $relation);
                $this->mytemplate->generate();
            }
        }
    }

    public function delete_old_files_ajax() {
        foreach ($this->file_fields as $field) {
            $resize_config = $this->fields_data[$field]->resize;
            $upload_path = $this->fields_data[$field]->upload['upload_path'];

            foreach ($resize_config as $file_field_name => $resize_data) {
                if (file_exists($upload_path . $this->input->post($file_field_name)) && !is_dir($upload_path . $this->input->post($file_field_name))) {
                    unlink($upload_path . $this->input->post($file_field_name));
                }
            }
        }
    }

    public function set_fields_data() {
        $merge_fields = $this->fields_data_custom();
        if (is_array($merge_fields) && !empty($merge_fields)) {
            foreach ($merge_fields as $key => $val) {
                $merge_fields[$key] = (Object) $val;
            }
        }

        $this->fields_data = array_merge($this->{$this->my_model}->get_fields_data(), $merge_fields);
        //echo "<pre>"; print_r($this->fields_data);die;
        $this->mytemplate->set_data(array('fields_data' => $this->fields_data, 'unset_fields' => $this->unset_fields));
    }

    public function fields_data_custom() {
        //"image":{"name":"image","type":"varchar","default":null,"max_length":"500","primary_key":0}
        array_push($this->file_fields, 'image');
        array_push($this->file_fields, 'image2');
        array_push($this->unset_fields, 'image_thumbs');
        array_push($this->unset_fields, 'image_thumbs2');
        array_push($this->unset_fields, 'image_thumbs3');
        array_push($this->unset_fields, 'image2_thumbs');
        array_push($this->unset_fields, 'image2_thumbs2');
        return array(
            'image' => array(
                'name' => 'image',
                'type' => 'file',
                'default' => null,
                'max_length' => '500',
                'primary_key' => 0,
                'return_image' => 'image_thumbs', //FOR JSON return
                'url_upload' => base_url("assets/img/test/"),
                'upload' => array(
                    'upload_path' => './assets/img/test/',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    //'max_size' => 0,
                    'encrypt_name' => FALSE,
                    'remove_spaces' => TRUE,
                    'overwrite' => FALSE
                ),
                'resize' => array(
                    'image' => array(
                        'sufiks_filename' => '',
                        'w' => '300',
                        'h' => '300',
                        'resize_ori_img' => TRUE
                    ),
                    'image_thumbs' => array(
                        'sufiks_filename' => '_thumb',
                        'w' => '100',
                        'h' => '100',
                        'resize_ori_img' => FALSE
                    ),
                    'image_thumbs2' => array(
                        'sufiks_filename' => '_thumb2',
                        'w' => '50',
                        'h' => '50',
                        'resize_ori_img' => FALSE
                    ),
                    'image_thumbs3' => array(
                        'sufiks_filename' => '_thumb3',
                        'w' => '25',
                        'h' => '25',
                        'resize_ori_img' => FALSE
                    )
                )
            ),
            'image2' => array(
                'name' => 'image2',
                'type' => 'file',
                'default' => null,
                'max_length' => '500',
                'primary_key' => 0,
                'return_image' => 'image2_thumbs', //FOR JSON return
                'url_upload' => base_url("assets/img/test/"),
                'upload' => array(
                    'upload_path' => './assets/img/test/',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    //'max_size' => 0,
                    'encrypt_name' => FALSE,
                    'remove_spaces' => TRUE,
                    'overwrite' => FALSE
                ),
                'resize' => array(
                    'image2' => array(
                        'sufiks_filename' => '',
                        'w' => '300',
                        'h' => '300',
                        'resize_ori_img' => TRUE
                    ),
                    'image2_thumbs' => array(
                        'sufiks_filename' => '_2thumb',
                        'w' => '100',
                        'h' => '100',
                        'resize_ori_img' => FALSE
                    ),
                    'image2_thumbs2' => array(
                        'sufiks_filename' => '_2thumb2',
                        'w' => '50',
                        'h' => '50',
                        'resize_ori_img' => FALSE
                    )
                )
            )
        );
    }

}

/* End of file test.php */
/* Location: ./application/controllers/backend/test.php */