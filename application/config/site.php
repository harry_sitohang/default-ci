<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$CI = &get_instance();

$config['encrypt_salt'] = 'mjiwernewnmNKnksndfmLKjkJKjiajsdjIias&#kKJDSkjkm!lklerk';
$config['user_auth_page'] = '/^dashboard$/i';

$config['administrator_page_path'] = 'admincms/';
$config['frontend_page_path'] = 'frontend/';



/**
 * @author harry
 * @desc config upload & resize "icon image"
 * @date 2 Jan 2014
 */
$config['url_upload_shortcut_icon'] = "{$CI->config->item('base_url')}assets/icon/";
$config['path_upload_icon_image'] = array(
    'upload_path' => './assets/icon/',
    'allowed_types' => 'gif|jpg|png|jpeg',
    //'max_size' => 0,
    'encrypt_name' => FALSE,
    'remove_spaces' => TRUE,
    'overwrite' => FALSE
);
$config['resize_icon_image'] = array(
    'w' => '100',
    'h' => '100'
);


/**
 * @author harry
 * @desc config upload & resize "slider image"
 * @date 2 Jan 2014
 */
$config['url_upload_slider'] = "{$CI->config->item('base_url')}assets/slider/";
$config['path_upload_slider_image'] = array(
    'upload_path' => './assets/slider/',
    'allowed_types' => 'gif|jpg|png|jpeg',
    //'max_size' => 0,
    'encrypt_name' => TRUE,
    'remove_spaces' => TRUE,
    'overwrite' => FALSE
);

$config['resize_slider_image'] = array(
      'big' => array(
        'w' => '618',
        'h' => '246'
    ),
    'thumbs' => array(
        'w' => '154',
        'h' => '61'
    )
);


unset($CI);


/* End of file site.php */
/* Location: ./application/config/site.php */
