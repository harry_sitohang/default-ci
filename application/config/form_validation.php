<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config = array(
    'login' => array(
        array(
            'field' => 'email',
            'label' => 'Email Address',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[100]|valid_email'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|callback_user_check|callback_user_is_activated|callback_user_is_banned'
        ),
        array(
            'field' => 'recaptcha_challenge_field',
            'label' => 'Recartcha Field',
            'rules' => 'xss_clean|required|callback_check_recaptcha'
        )
    ),
    'login_wot_recaptcha' => array(
        array(
            'field' => 'email',
            'label' => 'Email Address',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[100]|valid_email'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|callback_user_check|callback_user_is_activated|callback_user_is_banned'
        )
    ),
    'reset-password-frontend' => array(
        array(
            'field' => 'user_id',
            'label' => 'User id',
            'rules' => 'xss_clean|strip_tags|trim|required|numeric'
        ),
        array(
            'field' => 'old_password',
            'label' => 'Old Password',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|callback_check_old_password'
        ),
        array(
            'field' => 'new_password',
            'label' => 'New Password',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]'
        ),
        array(
            'field' => 'confirm_password',
            'label' => 'Confirm New Password',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|matches[new_password]'
        )
    ),
    'reset-password-backend' => array(
        array(
            'field' => 'user_id',
            'label' => 'User id',
            'rules' => 'xss_clean|strip_tags|trim|required|numeric'
        ),
        array(
            'field' => 'old_password',
            'label' => 'Old Password',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|callback_check_admin_password'
        ),
        array(
            'field' => 'new_password',
            'label' => 'New Password',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]'
        ),
        array(
            'field' => 'confirm_password',
            'label' => 'Confirm New Password',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|matches[new_password]'
        )
    ),
    'edit-setting' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|valid_email'
        ),
        array(
            'field' => 'mobile_phone_number',
            'label' => 'Mobile Phone Number',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|callback_check_mobile_phone_number'
        ),
        array(
            'field' => 'pin_bb',
            'label' => 'PIN BB',
            'rules' => 'xss_clean|strip_tags|trim|required|exact_length[8]'
        ),
        array(
            'field' => 'website_name',
            'label' => 'Website Name',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[100]'
        ),
        array(
            'field' => 'shortcut_icon',
            'label' => 'Shortcut Icon',
            'rules' => 'callback_check_shortcut_icon'
        )
    ),
    'edit-setting-wo-image' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|valid_email'
        ),
        array(
            'field' => 'mobile_phone_number',
            'label' => 'Mobile Phone Number',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[255]|callback_check_mobile_phone_number'
        ),
        array(
            'field' => 'pin_bb',
            'label' => 'PIN BB',
            'rules' => 'xss_clean|strip_tags|trim|required|exact_length[8]'
        ),
        array(
            'field' => 'website_name',
            'label' => 'Website Name',
            'rules' => 'xss_clean|strip_tags|trim|required|max_length[100]'
        )
    )
);

/* End of file form_validation.php */
/* Location: ./application/config/form_validation.php */