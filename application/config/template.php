<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/*
  |--------------------------------------------------------------------------
  | Website configuration
  |--------------------------------------------------------------------------
  |
  | this config will be used as default value in library : "libraries/Mytemplate.php
  | this config must be include in autoload "config/autoload.php" => $autoload['config'] = array('template');
  |
 */

$CI = &get_instance();

$config['minified_version'] = FALSE;
$config['web_title'] = 'DEFAULT';
$config['template_list'] = array(
    'default_frontend' => array(
        'template_path' => "template/frontend/default/",
        'template_view' => "template/frontend/default/index",
        'menu_view' => "template/frontend/default/menu",
        'footer_view' => "template/frontend/default/footer",
        'auto_css' => array(
            //sprintf('assets/bootstrap/css/bootstrap%s.css', $config['minified_version'] === TRUE ? '.min' : ''),
            //sprintf('assets/bootstrap/css/bootstrap-responsive%s.css', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/jquery-ui/themes/base/jquery.ui.all%s.css', $config['minified_version'] === TRUE ? '.min' : '')
        ),
        'auto_js' => array(
            sprintf('assets/js/jquery%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/bootstrap/js/bootstrap%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/jquery-ui/jquery-ui%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/js/site%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/js/alert.msg%s.js', $config['minified_version'] === TRUE ? '.min' : '')
        )
    ),
    'default_backend' => array(
        'template_path' => "template/backend/default/",
        'template_view' => "template/backend/default/index",
        'menu_view' => "template/backend/default/menu",
        'footer_view' => "template/backend/default/footer",
        'auto_css' => array(
            //sprintf('assets/bootstrap/css/bootstrap%s.css', $config['minified_version'] === TRUE ? '.min' : ''),
            //sprintf('assets/bootstrap/css/bootstrap-responsive%s.css', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/jquery-ui/themes/base/jquery.ui.all%s.css', $config['minified_version'] === TRUE ? '.min' : '')
        ),
        'auto_js' => array(
            sprintf('assets/js/jquery%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/bootstrap/js/bootstrap%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/jquery-ui/jquery-ui%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/js/site%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/js/alert.msg%s.js', $config['minified_version'] === TRUE ? '.min' : '')
        )
    ),
    'ace' => array(
        'template_path' => "template/backend/ace/",
        'template_view' => "template/backend/ace/index",
        'auto_css' => "",
        'auto_js' => array(
            sprintf('assets/js/site%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/js/alert.msg%s.js', $config['minified_version'] === TRUE ? '.min' : '')
        )
    ),
     'metronic' => array(
        'template_path' => "template/backend/metronic/",
        'template_view' => "template/backend/metronic/index",
        'auto_css' => "",
        'auto_js' => array(
            sprintf('assets/js/site%s.js', $config['minified_version'] === TRUE ? '.min' : ''),
            sprintf('assets/js/alert.msg%s.js', $config['minified_version'] === TRUE ? '.min' : '')
        )
    )
);

$config['template_frontend'] = "default_frontend"; //default_frontend
$config['template_backend'] = "metronic"; //default_backend|ace|metronic

$config['template_path_frontend'] = $config['template_list'][$config['template_frontend']]['template_path'];
$config['template_path_backend'] = $config['template_list'][$config['template_backend']]['template_path'];

unset($CI);

/* End of file template.php */
/* Location: ./application/config/template.php */
