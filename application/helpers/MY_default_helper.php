<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @desc this function used to array $options, contain dropdown-data used for form-helper-function : form_dropdown(array)
 * @param $arr array source
 * @param $field_key field-name which it value will be used as key-value in $options
 * @param $field_val field-name which it value will be used as value in $options
 * @desc for example view this code : http://localhost/core/kliktodaycms/bank_payment
 * @return array
 * @Auth : Harry Osmmar Sitohang 2012-10-10
 * 
 */
function create_form_dropdown_options($arr = array(), $field_key = '', $field_val = '') {
    $options = array();
    foreach ($arr as $field) {
        $options[$field[$field_key]] = $field[$field_val];
    }
    return $options;
}

/**
 * 
 * @desc this function used to format date
 * @param1 $date date no-default value
 * @param2 $format with default value 'd M Y' => '01 Jan 2012'
 * @return date-formated
 * @Auth : Harry Osmar Sitohang 2012-10-25
 * 
 */
function format_date($date, $format = 'd M Y') {
    return date($format, strtotime($date));
}

/**
 * 
 * @desc this function used to format number
 * @param $number sumeric
 * @return string
 * @Auth : Harry Osmmar Sitohang 2012-10-25
 * 
 */
function format_number($number) {
    return number_format($number, 0, ',', '.');
}


function convert_to_string_url($param){
//    $clean_url = preg_replace('/[-_,&]/', '', $param);
//    return trim(ucwords(preg_replace('/[ ]/', '-', $clean_url)), '-');
    $url = strtolower(preg_replace("/[^a-zA-Z0-9 _-]+/", "", strtolower(str_replace(array(' ', '&'), array('-', 'and'), trim($param)))));
    $url = preg_replace('#'.preg_quote("-", '#').'{2,}#', "-", $url);
    return $url;
}


/* End of file MY_default_helper.php */
/* Location: ./application/helper/MY_default_helper.php */