<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @desc this is a 'core model'
 * @author harry
 * @date : 2012 Nov 12
 */
class My_model extends CI_Model {

    public $table = ''; // TABLE NAME
    public $PK = ''; // PRIMARY KEY this value automatically set in $this->initialize() method

    /**
     * @desc initialize this class attribute, 'TABLE NAME' => $this->table AND 'PRIMARY KEY' => $this->pk
     * @void constructor 
     */

    function __construct($props = array()) {
        parent::__construct();
        if (count($props) > 0) {
            $this->initialize($props);
        }
    }

    /**
     * @desc initialize this class attribute, 'TABLE NAME' => $this->table AND 'PRIMARY KEY' => $this->pk
     * @void this function called in constructor method
     */
    function initialize($config = array()) {
        $defaults = array(
            'table' => '',
            'PK' => ''
        );

        foreach ($defaults as $key => $val) {
            $method = 'set_' . $key;
            if (method_exists($this, $method)) {
                $this->$method((isset($config[$key])) ? $config[$key] : $val);
            } else {
                $this->$key = (isset($config[$key])) ? $config[$key] : $val;
            }
        }
    }

    /**
     * @desc get table name
     * @return string
     */
    function get_table_name() {
        return $this->table;
    }

    /**
     * @desc get query by PK
     * @return query-object
     * @param1 $value => PK value, default value none required
     * @param2 $fields type @string or @array, define => columns to be selected @sting or @array default value '*'
     */
    function get_by_id($value, $fields = '') {
        if (!empty($fields)) {
            if (is_array($fields)) {
                $fields = implode(',', $fields);
            }
        } else {
            $fields = '*';
        }
        $this->db->select($fields);
        $this->db->from($this->table);
        $this->db->where($this->PK, $value);

        return $this->db->get();
    }

    /**
     * @desc get where query
     * @return query-object
     * @param1 $fields  type @string or @array define =>  columns to be selected @sting or @array default value '*'
     * @param2 $where type array(), where condition with formar array('field_name1' => value, 'field_name2' => value)
     */
    function get_where($fields = '', $where = array(), $order_column = '', $order_type = '') {
        if (!empty($fields)) {
            if (is_array($fields)) {
                $fields = implode(',', $fields);
            }
        } else {
            $fields = '*';
        }
        $this->db->select($fields);
        $this->db->from($this->table);

        if (!empty($where)) {
            $this->db->where($where);
        }

        if (!empty($order_column) && !empty($order_type)) {
            $this->db->order_by($order_column, $order_type);
        }

        return $this->db->get();
    }

    /**
     * @desc get where limited with limit and offset query
     * @return query-object
     * @param1 $fields type @string or @array define => columns to be selected @sting or @array default value '*'
     * @param2 $where @array(), where condition with formar array('field_name1' => value, 'field_name2' => value)
     * @param3 $offset type @int default value 0
     * @param4 $limit type @int default value 10
     */
    function get_where_limited($fields = '', $where = array(), $offset = 0, $limit = 10) {
        if (!empty($fields)) {
            if (is_array($fields)) {
                $fields = implode(',', $fields);
            }
        } else {
            $fields = '*';
        }
        $this->db->select($fields);
        $this->db->from($this->table);

        if (!empty($where)) {
            $this->db->where($where);
        }

        $this->db->limit($limit, $offset);

        return $this->db->get();
    }

    /**
     * @desc get where limited with limit and offset query
     * @void set field_name of PRIMARY KEY, this function called in this->initialize() method
     */
    function set_PK($param_pk = '') {
        $this->PK = empty($param_pk) ? $this->get_PK() : $param_pk;
    }

    /**
     * @desc get where limited with limit and offset query
     * @string get field_name of PRIMARY KEY call in this->set_PK() method
     */
    function get_PK() {
        if (!empty($this->table)) {
            $fields = $this->db->field_data($this->table);
            foreach ($fields as $field) {
                if ($field->primary_key == 1) {
                    break;
                }
            }
            return $field->name;
        } else {
            return FALSE;
        }
    }

    /**
     * @desc get fields 
     * @return array of string => get fields data
     */
    function get_fields() {
        return $this->db->list_fields($this->table);
    }

    /**
     * @desc get fields data
     * @return object
     */
    function get_fields_data() {
        return $this->db->field_data($this->table);
    }

    /**
     * @desc get total all  record 
     * @return int
     */
    function total_records() {
        return $this->db->count_all($this->table);
    }

    /**
     * 
     * @desc this function used to generate 'array' contains enumeration data from a field(field-name using as parameter)
     * @param $field field-name that have type 'enum'
     * @desc get enumeration value list from a field
     * 
     */
    public function get_enum_values($field, $table = '') {
        $table = ($table == '') ? $this->table : $table;
        $type = $this->db->query("SHOW COLUMNS FROM `{$table}` WHERE Field = '{$field}'")->row()->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);

//         preg_match('/\'(.*)\'/', $matches[1], $matches2);
//                 print_r($matches2); die;
        foreach (explode('\',\'', $matches[1]) as $value) {
            $enum[preg_replace('/[\']/', '', $value)] = preg_replace('/[\']/', '', $value);
        }
        return $enum;
    }

}

/* End of file My_model.php */
/* Location: ./application/models/My_model.php */