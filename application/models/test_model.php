<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Test_model extends Datatable_model {

    public $table = 'test'; // TABLE NAME

    function __construct() {
        parent::__construct(array(
            'table' => $this->table
        ));
    }


}

/* End of file test.php */
/* Location: ./application/models/test.php */