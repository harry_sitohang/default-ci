<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once  APPPATH . 'models/datatable_model'.EXT;

class Meta_model extends Datatable_model {

    function __construct() {
        parent::__construct(array(
            'table' => 'meta'
        ));
    }


}

/* End of file page.php */
/* Location: ./application/models/page.php */