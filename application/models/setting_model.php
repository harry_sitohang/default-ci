<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once  APPPATH . 'models/my_model'.EXT;

class Setting_model extends My_model {

    function __construct() {
        parent::__construct(array(
            'table' => 'setting'
        ));
    }


}

/* End of file setting_model.php */
/* Location: ./application/models/setting_model.php */