<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Slider_model extends Datatable_model {

    public $table = 'slider'; // TABLE NAME

    function __construct() {
        parent::__construct(array(
            'table' => $this->table
        ));
    }

    function datatable_select($data = array()) {
        $this->db->select('*');
    }

}

/* End of file slider_model.php */
/* Location: ./application/models/slider_model.php */