<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once  APPPATH . 'models/datatable_model'.EXT;

class Ci_sessions_model extends Datatable_model {

    function __construct() {
        parent::__construct(array(
            'table' => 'ci_sessions'
        ));
    }


}

/* End of file setting_model.php */
/* Location: ./application/models/setting_model.php */