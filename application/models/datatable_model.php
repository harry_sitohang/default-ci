<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once  APPPATH . 'models/my_model'.EXT;

/**
 * @desc this is a 'core model'
 * @author harry
 * @date : 2012 Nov 12
 */
class Datatable_model extends My_model {

    /**
     * @desc initialize this class attribute
     * @void constructor 
     */
    function __construct($props = array()) {
        if (count($props) > 0) {
            parent::__construct($props);
        }
    }

    /**
     * @desc datatable server processing
     * @return array
     */
    function dt_server_processing($data = array()) {
        //TOTAL ROWS
        $iTotalRecords = $this->total_records();
        
        //DISPLAY RECORDS
        $iTotalDisplayRecords = (empty($data['sWhere'])) ? $iTotalRecords : $this->dt_total_display_records($data);
        
        //RESULTSET
        $rResult = $this->dt_result($data, $iTotalDisplayRecords);
        
        //OUTPUT
        $output = array(
            "sEcho" => isset($_GET['sEcho']) ? intval($_GET['sEcho']) : 0,
            "iTotalRecords" => $iTotalRecords,
            "iTotalDisplayRecords" => $iTotalDisplayRecords,
            "aaData" => array()
        );
        
        
        return array(
            'output' => $output,
            'rResult' => $rResult
        );
    }

    /**
     * @desc calculate total display report with condition where
     * @return int
     */
    function dt_total_display_records($data = array()) {
        $this->datatable_select($data);
        $this->datatable_from();
        $this->datatable_where($data);
        return $this->db->count_all_results();
    }

    /**
     * @desc generate result datatable limited  + with condition search + order OVERRIDE THIS METHOD
     * @return object-result
     */
    function dt_result($data = array(), $iTotalDisplayRecords) {
        $this->datatable_select($data);
        $this->datatable_where($data);
        $this->datatable_from();
        $this->datatable_order($data);
        $this->datatable_limit($data, $iTotalDisplayRecords);
        $query = $this->db->get();
        $result =  $query->result();
        $query->free_result();
        return $result;
    }

    /**
     * @desc datatable select
     * @return query
     */
    function datatable_select($data = array()) {
        $this->db->select(str_replace(" , ", " ", implode("`, `", $data['aColumns'])));
    }

    /**
     * @desc datatable from
     * @return query
     */
    function datatable_from() {
        $this->db->from("{$this->table}");
    }

    /**
     * @desc datatable where
     * @return query
     */
    function datatable_where($data = array()) {
        if (!empty($data['sWhere'])) {
            $this->db->or_like($data['sWhere']);
        }
    }

    /**
     * @desc datatable order
     * @return query
     */
    function datatable_order($data = array()) {
        if ($data['order'] != "") {
            $this->db->order_by($data['order']);
        }
    }

    /**
     * @desc datatable limit
     * @return query
     */
    function datatable_limit($data = array(), $iTotalDisplayRecords) {
        $this->db->limit(isset($data['limit']) ? $data['limit'] : $iTotalDisplayRecords, isset($data['offset']) ? $data['offset'] : 0);
    }

}

/* End of file datatable_model.php */
/* Location: ./application/models/datatable_model.php */