<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once  APPPATH . 'models/datatable_model'.EXT;

class User_model extends Datatable_model {

    function __construct() {
        parent::__construct(array(
            'table' => 'users'
        ));
    }


}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */